﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BetWorldCup.Models.BetViewModels
{
    public class Bet
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BetId { get; set; }


        public int HomeScore { get; set; }

        public int AwayScore { get; set; }

        public virtual ResultType Result { get; set; }

        public virtual BetResult BetResult { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual Match Match { get; set; }
    }
}
