﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BetWorldCup.Models.BetViewModels
{
    public class Match
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MatchId { get; set; }

        [Required]
        [DisplayName("Mnożnik")]
        [DefaultValue(1)]
        public int Multiplier { get; set; }

        [Required]
        [DisplayName("Data rozpoczęcia meczu")]
        public DateTime StartDate { get; set; }
        
        [Required]
        public int LeagueId { get; set; }

        [DefaultValue(false)]
        [DisplayName("Zakończony")]
        public bool IsFinished { get; set; }

        public int HomeScore { get; set; }

        public int AwayScore { get; set; }
        
        [Required]
        public virtual Team HomeTeam { get; set; }

        [Required]
        public virtual Team AwayTeam { get; set; }


        public virtual ResultType Result { get; set; }

        public virtual ICollection<Bet> Bets { get; set; }
    }
}
