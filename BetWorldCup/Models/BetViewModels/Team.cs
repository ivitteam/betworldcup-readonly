﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BetWorldCup.Models.BetViewModels
{
    public class Team
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TeamId { get; set; }

        [Required]
        [DisplayName("Krótka nazwa")]
        [StringLength(4)]
        public string ShortName { get; set; }

        [Required]
        [DisplayName("Drużyna")]
        [StringLength(20)]
        public string LongName { get; set; }

        [Required]
        [DisplayName("Nazwa API")]
        public string APIName { get; set; }

        [Required]
        [DisplayName("API Id")]
        public string APIId{ get; set; }

        [Required]
        [DefaultValue(1)]
        public int Multiplier { get; set; }

        [DefaultValue(false)]
        public bool IsEliminated { get; set; }
    }
}
