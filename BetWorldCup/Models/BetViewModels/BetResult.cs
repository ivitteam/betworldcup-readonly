﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BetWorldCup.Models.BetViewModels
{
    public class BetResult
    {
        [Key]
        public string Type { get; set; }

        [Required]
        public int Points { get; set; }

        [NotMapped]
        public string TypeToString
        {
            get
            {
                switch (Type)
                {
                    case "NO_MATCH": return "Brak trafienia";
                    case "RESULT_MATCH": return "Trafiony rezultat";
                    case "SCORE_MATCH": return "Trafiony wynik";
                    default: return "nie rozpoznano typu";
                }
            }
        }
    }
}
