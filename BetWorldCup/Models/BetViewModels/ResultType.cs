﻿using System.ComponentModel.DataAnnotations;

namespace BetWorldCup.Models.BetViewModels
{
    public class ResultType
    {
        [Key]
        public string Type { get; set; }
    }
}
