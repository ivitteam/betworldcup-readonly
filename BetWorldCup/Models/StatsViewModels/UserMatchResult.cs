﻿namespace BetWorldCup.Models.StatsViewModels
{
    public class UserMatchResult
    {
        public string Result { get; set; }
        public string Username { get; set; }
        public int Points { get; set; }
        public string Score { get; set; }
        public bool IsLoggedUser { get; set; }
        public bool IsFunUser { get; set; }
    }
}
