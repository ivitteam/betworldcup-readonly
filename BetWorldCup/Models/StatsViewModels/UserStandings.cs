﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BetWorldCup.Models.StatsViewModels
{
    public class UserStandings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }

        public DateTime DateFor { get; set; }
        
        public virtual ApplicationUser User { get; set; }

        public int Points { get; set; }

        public int Position { get; set; }
    }
}
