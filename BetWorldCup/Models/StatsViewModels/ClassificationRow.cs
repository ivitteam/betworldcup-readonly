﻿namespace BetWorldCup.Models.StatsViewModels
{
    public class ClassificationRow
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Position { get; set; }
        public int Points { get; set; }
        public bool IsLoggedUser { get; set; }
        public bool IsFunUser { get; set; }
    }
}
