﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BetWorldCup.Models.BetViewModels;
using BetWorldCup.Models.StatsViewModels;
using Microsoft.AspNetCore.Identity;

namespace BetWorldCup.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        [StringLength(20)]
        public string Name { get; set; }

        public bool FunAccount { get; set; }

        public virtual ICollection<Bet> Bets { get; set; }

        public virtual ICollection<UserStandings> UserStandings { get; set; }

        public int Points { get; set; }

        public int Position { get; set; }
    }
}
