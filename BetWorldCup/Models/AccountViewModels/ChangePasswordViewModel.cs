﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BetWorldCup.Models.AccountViewModels
{
    public class ChangePasswordViewModel
    {

        [Required]
        [EmailAddress]
        [DisplayName(@"Adres e-mail:")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Hasło musi zawierać conajmniej 8 znaków w tym cyfrę oraz dwa różne znaki", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [DisplayName(@"Stare Hasło")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Hasło musi zawierać conajmniej 8 znaków w tym cyfrę oraz dwa różne znaki", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [DisplayName(@"Nowe Hasło")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [DisplayName(@"Potwierdź hasło")]
        [Compare("Password", ErrorMessage = "Hasła nie pasują do siebie.")]
        public string ConfirmPassword { get; set; }
    }
}
