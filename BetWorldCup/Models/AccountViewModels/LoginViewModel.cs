﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BetWorldCup.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        [DisplayName(@"Adres e-mail:")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName(@"Hasło:")]
        public string Password { get; set; }

        [Display(Name = "Zapamiętaj mnie:")]
        public bool RememberMe { get; set; }
    }
}
