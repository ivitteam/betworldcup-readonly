﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BetWorldCup.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        [DisplayName(@"E-mail")]
        public string Email { get; set; }

        [Required]
        [DisplayName(@"Nazwa użytkownika")]
        public string Name { get; set; }
    }
}