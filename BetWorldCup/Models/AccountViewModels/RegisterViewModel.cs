﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BetWorldCup.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [DisplayName(@"E-mail")]
        public string Email { get; set; }

        [Required]
        [DisplayName(@"Nazwa użytkownika")]
        public string Name { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Hasło musi zawierać conajmniej 8 znaków w tym cyfrę oraz dwa różne znaki", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [DisplayName("Hasło")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Potwierdź hasło")]
        [Compare("Password", ErrorMessage = "Hasła nie pasują do siebie.")]
        public string ConfirmPassword { get; set; }
    }
}
