﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BetWorldCup.Models
{
    public class ChatRow
    {
        public string ChatUser { get; set; }
        public string ChatMessage { get; set; }
        public string ChatTime { get; set; }
    }

    public class ChatDbRow
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string ChatUser { get; set; }
        public string ChatMessage { get; set; }
        public DateTime ChatTime { get; set; }
    }
}
