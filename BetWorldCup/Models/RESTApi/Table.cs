﻿using System.Collections.Generic;

namespace BetWorldCup.Models.RESTApi.Table
{
    public class Table
    {
        public string league_id { get; set; }
        public string season_id { get; set; }
        public string name { get; set; }
        public string rank { get; set; }
        public string points { get; set; }
        public string matches { get; set; }
        public string goal_diff { get; set; }
        public string goals_scored { get; set; }
        public string goals_conceded { get; set; }
        public string lost { get; set; }
        public string drawn { get; set; }
        public string won { get; set; }

        public string LongName { get; set; }
        public string ShortName { get; set; }
        public int TeamId { get; set; }
    }

    public class Data
    {
        public List<Table> table { get; set; }
    }

    public class RootObject
    {
        public bool success { get; set; }
        public Data data { get; set; }
    }
}
