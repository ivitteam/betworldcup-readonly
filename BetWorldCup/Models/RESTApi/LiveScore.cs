﻿using System.Collections.Generic;

namespace BetWorldCup.Models.RESTApi
{
    public class Match
    {
        public string id { get; set; }
        public string home_name { get; set; }
        public string away_name { get; set; }
        public string score { get; set; }
        public string ht_score { get; set; }
        public string ft_score { get; set; }
        public string et_score { get; set; }
        public string time { get; set; }
        public string league_id { get; set; }
        public string status { get; set; }
        public string added { get; set; }
        public string last_changed { get; set; }
        public string home_id { get; set; }
        public string away_id { get; set; }
        public object events { get; set; }
        public string league_name { get; set; }
    }

    public class Data
    {
        public List<Match> match { get; set; }
    }

    public class Response
    {
        public bool success { get; set; }
        public Data data { get; set; }
    }
}
