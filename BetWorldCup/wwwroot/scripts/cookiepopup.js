﻿$(function () {
    // Cookies
    var options = {
        title: "Ta strona wykorzystuje pliki cookie",
        text: "Używamy informacji zapisanych za pomocą plików cookies w celu zapewnienia maksymalnej wygody w korzystaniu z naszego serwisu. Mogą też korzystać z nich współpracujące z nami firmy badawcze oraz reklamowe. Jeżeli wyrażasz zgodę na zapisywanie informacji zawartej w cookies kliknij Akceptuj w prawym górnym rogu tej informacji. Jeśli nie wyrażasz zgody, ustawienia dotyczące plików cookies możesz zmienić w swojej przeglądarce.",
        theme: "dark",
        learnMore: true,
        position: "bottom",
        onAccept: acceptCallbackFunction
    };

    var cookie = $.acceptCookies(options);
    updateCodeArea();

    $('.clear-button').click(function (e) {
        e.preventDefault();
        $('#cookie-popup-container').remove();
        document.cookie = 'cookiesAccepted=; path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        cookie = $.acceptCookies(options);
        $(".cookie-indicator").removeClass("badge-success").addClass("badge-danger").text("No cookie found");
        updateCodeArea();
    });

    $('.theme-button').click(function (e) {
        e.preventDefault();
        $('#cookie-popup-container').remove();
        options.theme = $(this).data("theme").replace("theme-", "");
        cookie = $.acceptCookies(options);
        updateCodeArea();
    });

    $('.position-button').click(function (e) {
        e.preventDefault();
        $('#cookie-popup-container').remove();

        var position = $(this).data("position");
        options.position = position;
        cookie = $.acceptCookies(options);
        updateCodeArea();
    });

    $('#btnCustomize').click(function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: ($('.theme-buttons').offset().top)
        }, 500);
    });

    $('.option-button').click(function (e) {
        e.preventDefault();
        $('#cookie-popup-container').remove();
        var option = $(this).data("option");

        if (option == "default") {
            options = {
                title: "This website uses cookies",
                text: "By using this site, you agree to our use of cookies.",
                theme: "dark",
                learnMore: true,
                position: "bottom",
                onAccept: acceptCallbackFunction
            }
        }
        else if (option == "nolearnbutton")
            options.learnMore = false;
        else if (option == "customtext") {
            if ($("#customHeader").val() != "")
                options.title = $("#customHeader").val();
            if ($("#customSubHeader").val() != "")
                options.text = $("#customSubHeader").val();
            if ($("#customAccept").val() != "")
                options.acceptButtonText = $("#customAccept").val();
            if ($("#customLearnMore").val() != "")
                options.learnMoreButtonText = $("#customLearnMore").val();
            if ($("#customLearnMoreInfo").val() != "")
                options.learnMoreInfoText = $("#customLearnMoreInfo").val();
        }

        cookie = $.acceptCookies(options);
        updateCodeArea();
    });

    if (getCookie("cookiesAccepted"))
        $(".cookie-indicator").removeClass("badge-danger").addClass("badge-success").text("Cookie saved");
    else
        $(".cookie-indicator").removeClass("badge-success").addClass("badge-danger").text("No cookie found");

    function updateCodeArea() {
        var code =
            "var options = " +
            JSON.stringify(options, null, 5) +
            ";\n" +
            "$.acceptCookies(options);";

        $('#currentOptions').val(code);
        $("#currentOptions").height(0);
        $("#currentOptions").height($("#currentOptions").scrollHeight);
    }
});

var acceptCallbackFunction = function () {
    $(".cookie-indicator").removeClass("badge-danger").addClass("badge-success").text("Cookie saved");
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
(function ($) {
    "use strict";

    const cookiePopupHtml = '<div id="cookie-popup-container">' +
        '<div class="cookie-popup" style="display: none;">' +
        '<div class="cookie-popup-inner">' +
        '<div class="cookie-popup-left">' +
        '<div class="cookie-popup-headline">This website uses cookies</div>' +
        '<div class="cookie-popup-sub-headline">By using this site, you agree to our use of cookies.</div>' +
        '</div>' +

        '<div class="cookie-popup-right">' +
        '<a href="#" class="cookie-popup-accept-cookies">Akceptuj</a>' +
        '<a href="#" class="cookie-popup-learn-more">Więcej</a>' +
        '</div>' +
        '</div>' +
        '<div class="cookie-popup-lower" style="display: none;">' +
        '1. Serwis zbiera w sposób automatyczny tylko informacje zawarte w plikach cookies.</br>2. Pliki(cookies) są plikami tekstowymi, które przechowywane są w urządzeniu końcowym użytkownika serwisu.Przeznaczone są do korzystania ze stron serwisu.Przede wszystkim zawierają nazwę strony internetowej swojego pochodzenia, swój unikalny numer, czas przechowywania na urządzeniu końcowym.</br>3. Operator serwisu ivIT jest podmiotem zamieszczającym na urządzeniu końcowym swojego użytkownika pliki cookies oraz mającym do nich dostęp.</br>4. Operator serwisu wykorzystuje pliki(cookies) w celu:dopasowania zawartości strony internetowej do indywidualnych preferencji użytkownika, przede wszystkim pliki te rozpoznają jego urządzenie, aby zgodnie z jego preferencjami wyświetlić stronę;przygotowywania statystyk pomagających poznaniu preferencji i zachowań użytkowników, analiza tych statystyk jest anonimowa i umożliwia dostosowanie zawartości i wyglądu serwisu do panujących trendów, statystyki stosuje się też do oceny popularności strony; możliwości logowania do serwisu;utrzymania logowania użytkownika na każdej kolejnej stronie serwisu.</br>5. Serwis stosuje dwa zasadnicze rodzaje plików(cookies) - sesyjne i stałe.Pliki sesyjne są tymczasowe, przechowuje się je do momentu opuszczenia strony serwisu(poprzez wejście na inną stronę, wylogowanie lub wyłączenie przeglądarki).Pliki stałe przechowywane są w urządzeniu końcowym użytkownika do czasu ich usunięcia przez użytkownika lub przez czas wynikający z ich ustawień.</br> Użytkownik może w każdej chwili dokonać zmiany ustawień swojej przeglądarki, aby zablokować obsługę plików(cookies) lub każdorazowo uzyskiwać informacje o ich umieszczeniu w swoim urządzeniu.Inne dostępne opcje można sprawdzić w ustawieniach swojej przeglądarki internetowej.Należy pamiętać, że większość przeglądarek domyślnie jest ustawione na akceptację zapisu plików(cookies)w urządzeniu końcowym.</br>7. Operator Serwisu informuje, że zmiany ustawień w przeglądarce internetowej użytkownika mogą ograniczyć dostęp do niektórych funkcji strony internetowej serwisu.</br>8. Pliki(cookies) z których korzysta serwis(zamieszczane w urządzeniu końcowym użytkownika) mogą być udostępnione jego partnerom oraz współpracującym z nim reklamodawcą.</br>9. Informacje dotyczące ustawień przeglądarek internetowych dostępne są w jej menu(pomoc) lub na stronie jej producenta.' +
        '</div>' +
        '</div>' +
        '</div>';

    var onAccept;

    $.extend({
        acceptCookies: function (options) {
            var cookiesAccepted = getCookie("cookiesAccepted");

            if (!cookiesAccepted) {
                var cookiePopup = $(cookiePopupHtml);
                var position = "bottom";

                if (options != undefined) {
                    position = options.position != undefined ? options.position : "bottom";

                    if (options.title != undefined)
                        cookiePopup.find('.cookie-popup-headline').text(options.title);
                    if (options.text != undefined)
                        cookiePopup.find('.cookie-popup-sub-headline').text(options.text);
                    if (options.acceptButtonText != undefined)
                        cookiePopup.find(".cookie-popup-accept-cookies").text(options.acceptButtonText);
                    if (options.learnMoreButtonText != undefined)
                        cookiePopup.find(".cookie-popup-learn-more").text(options.learnMoreButtonText);
                    if (options.learnMoreInfoText != undefined)
                        cookiePopup.find(".cookie-popup-lower").text(options.learnMoreInfoText);
                    if (options.theme != undefined)
                        cookiePopup.addClass("theme-" + options.theme);
                    if (options.onAccept != undefined)
                        onAccept = options.onAccept;

                    if (options.learnMore != undefined) {
                        if (options.learnMore == false)
                            cookiePopup.find(".cookie-popup-learn-more").remove();
                    }

                }

                cookiePopup.find('.cookie-popup').addClass("position-" + position);
                $('body').append(cookiePopup);
                $('.cookie-popup').slideToggle();
            }
        }
    });

    $(document).on('click', '.cookie-popup-accept-cookies', function (e) {
        e.preventDefault();
        saveCookie();
        $('.cookie-popup').slideToggle();
        if (typeof onAccept === "function")
            onAccept();
    })
    //$(".cookie-popup-learn-more").click(function () {
    //    // Holds the product ID of the clicked element
    //    //e.preventDefault();
    //    $('.cookie-popup-lower').slideToggle();
    //});

    $(document).ready(function () {
    $('.cookie-popup-learn-more').click(function (e) {
        $('.cookie-popup-lower').slideToggle();

    });
    });

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function saveCookie() {
        var expires = "expires=01/01/2099"
        document.cookie = "cookiesAccepted=true; " + expires + "; path=/";
    }

}(jQuery));