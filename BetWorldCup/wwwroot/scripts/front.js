$(document).ready(function () {
    'use strict';
    
    // ------------------------------------------------------- //
    // Tooltips
    // ------------------------------------------------------ //-
    $('[data-toggle="tooltip"]').tooltip();

    // ------------------------------------------------------- //
    // Search Box
    // ------------------------------------------------------ //
    $('#search').on('click', function (e) {
        e.preventDefault();
        $('.search-box').fadeIn();
    });
    $('.dismiss').on('click', function () {
        $('.search-box').fadeOut();
    });

    // ------------------------------------------------------- //
    // Card Close
    // ------------------------------------------------------ //
    $('.card-close a.remove').on('click', function (e) {
        e.preventDefault();
        $(this).parents('.card').fadeOut();
    });


    // ------------------------------------------------------- //
    // Adding fade effect to dropdowns
    // ------------------------------------------------------ //
    $('.dropdown').on('show.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeIn();
    });
    $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeOut();
    });


    // ------------------------------------------------------- //
    // Sidebar Functionality
    // ------------------------------------------------------ //
    if ($(window).outerWidth() > 1199) {
        $('#chat-btn').toggleClass('active');
    }
    $('#chat-btn').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');

        $('.side-navbar').toggleClass('shrinked');
        $('.content-inner').toggleClass('chat-actived');
        $(document).trigger('sidebarChanged');
        var stats = $('.right-sidebar');

        if ($(window).outerWidth() > 1199) {
            if ($('#chat-btn').hasClass('active')) {
                //$('.navbar-header .brand-small').hide();
                //$('.navbar-header .brand-big').show();
                $('.style-switch').show();
                stats.removeClass('narrow');
                $('.right-sidebar h1').hide();
                $('#right-sidebar--stats__table').hide();
                $('#legend').hide();
            } else {
                //$('.navbar-header .brand-small').show();
                //$('.navbar-header .brand-big').hide();
                $('.style-switch').hide();
                stats.toggleClass('narrow');
                $('.right-sidebar h1').show();
                $('#right-sidebar--stats__table').show();
                $('#legend').show();

            }
        }
        else {
            $('.navbar-header .brand-small').show();
            $('.navbar-header .brand-big').hide();
        }
        if ($(window).outerWidth() <= 1199) {
            $('.navbar-header .brand-small').show();

            if ($('#chat-btn').hasClass('active')) {
                $('.style-switch').show();
                $('.right-sidebar').hide();
                $('nav.side-navbar .sidebar-header .title').show();
                $('#right-sidebar--stats').removeClass('onscroll');
                $('#chat-btn').removeClass('onscroll');
                $('#chat-btn').removeClass('onscroll--chat');
                $('body').css('overflow-y', 'hidden');
            }
            else {
                $('.right-sidebar').show();
                $('.style-switch').hide();
                $('body').css('overflow-y', 'scroll');
                $('nav.side-navbar .sidebar-header .title').hide();
                if ($(window).scrollTop() >= 70) {
                    $('#right-sidebar--stats').addClass('onscroll');
                    $('#chat-btn').addClass('onscroll');
                    $('#chat-btn').addClass('onscroll--chat');
                }

            }
            
        }
    });

    $('#right-sidebar--stats').on('click', function (e) {
        e.preventDefault();
        var stats = $('.right-sidebar');
        if ($(window).outerWidth() > 1199) {
            $('#chat-btn').toggleClass('active');
            $('.content-inner').toggleClass('chat-actived');
            if (stats.hasClass('narrow')) {
                stats.removeClass('narrow');
                $('.right-sidebar h1').hide();
                $('#right-sidebar--stats__table').hide();
                $('#legend').hide();
                $('.side-navbar').removeClass('shrinked');
                $('.style-switch').show();
            }
            else {
                if ($('#chat-btn').hasClass('active') && $('.side-navbar').hasClass('shrinked')) {
                    stats.toggleClass('narrow');
                    $('.right-sidebar h1').show();
                    $('#right-sidebar--stats__table').show();
                    $('#legend').show();
                }
                else {
                    stats.toggleClass('narrow');
                    $('.right-sidebar h1').show();
                    $('#right-sidebar--stats__table').show();
                    $('#legend').show();
                    $('#chat-btn').removeClass('active');
                    $('.content-inner').removeClass('active');
                    $('.side-navbar').toggleClass('shrinked');
                    $('.style-switch').hide();
                }
            }
        }
        else {
            if (stats.hasClass('narrow')) {
                stats.removeClass('narrow');
                $('body').css('overflow-y', 'scroll');
                if ($(window).scrollTop() >= 70) {
                    $('#right-sidebar--stats').addClass('onscroll');
                }
                $('nav.right-sidebar .title h1').css('margin-left', '30px');
                $('nav.right-sidebar .sidebar-header').css('padding', '15px 0');
                $('nav.right-sidebar .sidebar-header').css('margin-left', '-50px');
            }
            else {
                stats.toggleClass('narrow');
                $('body').css('overflow-y', 'hidden');
                $('#right-sidebar--stats').removeClass('onscroll');
                $('nav.right-sidebar .title h1').css('margin-left', '-35px');
                $('nav.right-sidebar .sidebar-header').css('padding', '15px 15px');
                $('nav.right-sidebar .sidebar-header').css('margin-left', '0px');
            }
        }
    });

    // ------------------------------------------------------- //
    // On scroll action for sidebars
    // ------------------------------------------------------ //
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 70) {
            $('#chat-btn').addClass('onscroll');
            $('#chat-btn').addClass('onscroll--chat');
            $('#right-sidebar--stats').addClass('onscroll');
        }
        else {
            $('#chat-btn').removeClass('onscroll');
            $('#chat-btn').removeClass('onscroll--chat');
            $('#right-sidebar--stats').removeClass('onscroll');
        }
    });
    
    // ------------------------------------------------------- //
    // Universal Form Validation
    // ------------------------------------------------------ //

    $('.form-validate').each(function () {
        $(this).validate({
            errorElement: "div",
            errorClass: 'is-invalid',
            validClass: 'is-valid',
            ignore: ':hidden:not(.summernote, .checkbox-template, .form-control-custom),.note-editable.card-block',
            errorPlacement: function (error, element) {
                // Add the `invalid-feedback` class to the error element
                error.addClass("invalid-feedback");
                console.log(element);
                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.siblings("label"));
                }
                else {
                    error.insertAfter(element);
                }
            }
        });

    });
    // ------------------------------------------------------- //
    // Checkboxes Inputs - value
    // ------------------------------------------------------ //
    $(".checkbox-template").on("click",
        function() {
            var $this = $(this);
            if ($this.val() == "True") {
                $this.val("False");
            } else {
                $this.val("True");
            }
        });


    // ------------------------------------------------------- //
    // Material Inputs
    // ------------------------------------------------------ //

    var materialInputs = $('input.input-material');

    // activate labels for prefilled values
    materialInputs.filter(function () { return $(this).val() !== ""; }).siblings('.label-material').addClass('active');

    // move label on focus
    materialInputs.on('focus', function () {
        $(this).siblings('.label-material').addClass('active');
    });

    // remove/keep label on blur
    materialInputs.on('blur', function () {
        $(this).siblings('.label-material').removeClass('active');

        if ($(this).val() !== '') {
            $(this).siblings('.label-material').addClass('active');
        } else {
            $(this).siblings('.label-material').removeClass('active');
        }
    });

    // ------------------------------------------------------- //
    // Footer 
    // ------------------------------------------------------ //   

    var contentInner = $('.content-inner');

    $(document).on('sidebarChanged', function () {
        adjustFooter();
    });

    $(window).on('resize', function () {
        adjustFooter();
    });

    function adjustFooter() {
        var footerBlockHeight = $('.main-footer').outerHeight();
        contentInner.css('padding-bottom', footerBlockHeight + 'px');
    }

    // ------------------------------------------------------- //
    // External links to new window
    // ------------------------------------------------------ //
    $('.external').on('click', function (e) {

        e.preventDefault();
        window.open($(this).attr("href"));
    });

    // ------------------------------------------------------- //
    // Transition Placeholders
    // ------------------------------------------------------ //
    $("input.input-material").on("focus", function () {
        $(this).siblings(".label-material").addClass("active");
    });

    $("input.input-material").on("blur", function () {
        $(this).siblings(".label-material").removeClass("active");

        if ($(this).val() !== "") {
            $(this).siblings(".label-material").addClass("active");
        } else {
            $(this).siblings(".label-material").removeClass("active");
        }
    });

    setTimeout(function () {

        var allInputs = $(":input.input-material");
        allInputs.each(function (index, element) {
            ChangeLabelOnInputAutofill($(element).attr("name"));
        });
    }, 25);
});

function ChangeLabelOnInputAutofill(inputName) {
    var stringInput = "#" + inputName;
    var input = $(stringInput).addClass(":-webkit-autofill");
    input.siblings(".label-material").removeClass("active");
    input.siblings(".label-material").addClass("active");
}

// ------------------------------------------------------- //
// Obs�uga przycisku SAVE 
// ------------------------------------------------------ //

function StartPosting($button) {

    var $i = $('.fa.fa-save');
    if ($button.hasClass('active') || $button.hasClass('success') || $button.hasClass('fail')) {
        return false;
    }
    $button.addClass('active');
    $button.addClass('loader');
    $button.find($i).hide();

    return true;
}

function PostingError($button) {
    var $i = $('.fa.fa-save');
    setTimeout(function() {
        $button.removeClass('loader active');
        $button.append('<i class="fa fa-exclamation" style="color: #fff"></i>');
        $button.addClass('fail animated pulse');
    }, 2000);
    setTimeout(function() {
        $button.removeClass('fail animated pulse');
        $button.find($i).show();
        $('.fa.fa-exclamation').remove();
        $button.blur();
    }, 2900);
}

function PostingSuccess($button) {
    var $i = $('.fa.fa-save');
    setTimeout(function() {
        $button.removeClass('loader active');
        $button.append('<i class="fa fa-check" style="color: #fff"></i>');
        $button.addClass('success animated pulse');
    }, 2000);
    setTimeout(function() {
        $button.removeClass('success animated pulse');
        $button.find($i).show();
        $('.fa.fa-check').remove();
        $button.blur();
    }, 2900);
}

// ------------------------------------------------------- //
// KONIEC - Obs�uga przycisku SAVE 
// ------------------------------------------------------ //

// ------------------------------------------------------- //
// TOASTER - AJAX response
// ------------------------------------------------------ //

function ShowToaster(message, type) {

    // ------------------------------------------------------- //
    // Konfiguracja toastera
    // ------------------------------------------------------ //
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    switch (type) {
    case "error":
        toastr.error(message);
    case "warning":
        toastr.warning(message);
    case "info":
        toastr.info(message);
    case "success":
        toastr.success(message);
    }
}

// ------------------------------------------------------- //
// KONIEC - TOASTER - AJAX response
// ------------------------------------------------------ //

function RandomColorGenerator(counter, pallete) {
    var color = pallete[counter.value];
    counter.value = counter.value + 1;
    return color;
}