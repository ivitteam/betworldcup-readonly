﻿window.onload = function (data) {
    var LoginName = $("#appname").val();
    var userName = LoginName;
    var protocol = location.protocol === "http:" ? "wss:" : "wss:";
    var wsUri = protocol + "//" + window.location.host;
    var socket = new WebSocket(wsUri);
    
    socket.onopen = function(e) {
        $.ajax({
            url: $("#GetChatMessages").val(),
            type: "POST",
            cache: false,
            success: function(data) {
                if (data != undefined) {
                    for (var i = 0; i < data.length; i++) {
                        $('#logins').append("<span class='logins--span'><div class='row'><div class='col-6'>" +
                            data[i].chatUser +
                            "</div><div class='col-6 logins--span__date'><small class='logins--span__small'>" +
                            data[i].chatTime +
                            "</small></div></div><div class='row'><h5 class='logins--span__h5'>" +
                            data[i].chatMessage +
                            "</h5></div>" +
                            "<hr>");
                        $('#datenow').append("<small>" + data[i].chatTime + "</small>");
                        $("#scrollDown").animate({ scrollTop: $('#scrollDown').prop("scrollHeight") }, 300);
                    }
                }
            }
        });
    };

    socket.onclose = function (e) {
    };

    socket.onmessage = function (e) {
        var Login = e.data.substr(0, e.data.indexOf('??##')); 
        var Messages = e.data.substr(e.data.indexOf('??##') + 4);
        var currentDate = new Date();
        currentDate = (currentDate.getHours() < 10 ? '0' : '') + currentDate.getHours()  
            + ":" + (currentDate.getMinutes() < 10 ? '0' : '') + currentDate.getMinutes() 
            + ":" + (currentDate.getSeconds() < 10 ? '0' : '') + currentDate.getSeconds();
        if (Login != $("#appname").val()) {
            $(".fa.fa-comments.fa-2x").css("color", "#2b90d9");
        }
        $("#scrollDown").animate({ scrollTop: $('#scrollDown').prop("scrollHeight") }, 1000);
        $('#logins').append("<span class='logins--span'><div class='row'><div class='col-6'>" + Login + "</div><div class='col-6 logins--span__date'><small class='logins--span__small'>" + currentDate + "</small></div></div><div class='row'><h5 class='logins--span__h5'>" + Messages + "</h5></div>" + "<hr>");
        $('#datenow').append("<small>" + currentDate + "</small>");

        var data =
        {
            ChatUser: Login,
            ChatMessage: Messages,
            ChatTime: currentDate
        };

        $.ajax({
            url: $("#UrlAddMessage").val(),
            type: "POST",
            cache: false,
            data: {
                model: data
                },
            });

    };
    socket.onerror = function (e) {
        console.error(e.data);
    };

    $('#MessageField').click(function (e) {
        $(".fa.fa-comments.fa-2x").css("color", "#686a76");
    })

    $('#MessageField').keypress(function (e) {
        $(".fa.fa-comments.fa-2x").css("color", "#686a76");
        if (e.which != 13) {
            return;
        }
        e.preventDefault();

        var message = userName + "??##" + $('#MessageField').val();
        socket.send(message);
        $('#MessageField').val('');
    });

    $("#sendmes").on("click", function () {
        var message = userName + "??##" + $('#MessageField').val();
        socket.send(message);
        $('#MessageField').val('');
    })

};