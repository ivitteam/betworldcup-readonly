using System.Text.Encodings.Web;
using System.Threading.Tasks;


// ReSharper disable once CheckNamespace
namespace BetWorldCup.Services
{
    public static class EmailSenderExtensions
    {
        public static Task SendEmailConfirmationAsync(this IEmailSender emailSender, string email, string link)
        {
            return emailSender.SendEmailAsync(email, "Potwierd� sw�j e-mail.",
                $"Prosz� kliknij nast�puj�cy link, �eby aktywowa� konto: <a href='{HtmlEncoder.Default.Encode(link)}'>LINK</a>");
        }
    }
}
