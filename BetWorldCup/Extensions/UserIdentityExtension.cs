﻿using System.Linq;
using System.Security.Claims;

namespace BetWorldCup.Extensions
{
    public static class UserIdentityExtension
    {
        public static string AppName(this ClaimsPrincipal user)
        {
            return user.Identity.IsAuthenticated ? user.Claims.FirstOrDefault(v => v.Type == ClaimTypes.GivenName)?.Value : "brak nazwy użytkownika";
        }

        public static bool FunUser(this ClaimsPrincipal user)
        {
            return bool.Parse(user.Identity.IsAuthenticated
                ? user.Claims.FirstOrDefault(v => v.Type == ClaimTypes.System)?.Value
                : "false");
        }
    }
}
