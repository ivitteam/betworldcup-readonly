﻿using System;
using System.Collections.Generic;
using System.Linq;
using BetWorldCup.Helpers;
using BetWorldCup.Models;
using BetWorldCup.Models.BetViewModels;
using BetWorldCup.Services.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BetWorldCup.Services
{
    public class AdminService : IAdminService
    {
        private readonly IMatchRepository _matchRepository;
        private readonly IBetRepository _betRepository;
        private readonly IRepository<ResultType> _resultRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IStandingsRepository _standingsRepository;
        private readonly IStatsService _statsService;

        public AdminService(
            IMatchRepository matchRepository,
            IBetRepository betRepository,
            IRepository<ResultType> resultRepository,
            IRepository<Team> teamRepository,
            UserManager<ApplicationUser> userManager,
            IStandingsRepository standingsRepository,
            IStatsService statsService)
        {
            _matchRepository = matchRepository;
            _betRepository = betRepository;
            _resultRepository = resultRepository;
            _teamRepository = teamRepository;
            _userManager = userManager;
            _standingsRepository = standingsRepository;
            _statsService = statsService;
        }

        public IEnumerable<Match> GetMatchesForStartScreen()
        {
            return _matchRepository.GetMatchesForAdminStartScreen(true);
        }

        public IEnumerable<Match> GetMatchesForAdminModify()
        {
            return _matchRepository.GetMatchesForAdminModify(true);
        }

        public void UpdateMatchScore(Match model)
        {
            var match = _matchRepository.GetById(model.MatchId);
            match.HomeScore = model.HomeScore;
            match.AwayScore = model.AwayScore;
            match.IsFinished = model.IsFinished;
            match.Result = _resultRepository.GetById(BetHelpers.GetResultType(model.HomeScore, model.AwayScore));
            _matchRepository.InsertOrUpdate(match);
            _betRepository.UpdateBetResults(match);
        }

        public IEnumerable<Team> GetNotEliminatedTeams() => _teamRepository.GetAll(true).Where(p => !p.IsEliminated);

        public IEnumerable<Team> GetTeams() => _teamRepository.GetAll(false);

        public Match AddMatch(Match model) => _matchRepository.InsertOrUpdate(model);

        public Team GetTeamById(int id) => _teamRepository.GetById(id);

        public void DeleteMatch(int id) => _matchRepository.Delete(id);

        public Match GetMatch(int id) => _matchRepository.GetSingleMatch(true, id);

        public void UpdateEliminatedStatus(Team model)
        {
            var team = _teamRepository.GetById(model.TeamId);
            team.IsEliminated = model.IsEliminated;
            _teamRepository.InsertOrUpdate(team);
        }
        
        public void UpdateFunUser(ApplicationUser model)
        {
            var user = _userManager.FindByIdAsync(model.Id).Result;
            user.FunAccount = model.FunAccount;
            var unused = _userManager.UpdateAsync(user).Result;
        }
        
        public void DeleteUser(string id)
        {
            var user = _userManager.Users.Include(p => p.Bets).Include(p => p.UserStandings).SingleOrDefault(p => p.Id == id);
            if (user == null) return;
            
            _betRepository.MultipleDelete(user.Bets.Select(p => (object)p.BetId));
            _standingsRepository.MultipleDelete(user.UserStandings.Select(p => (object)p.Id));
            var unused = _userManager.DeleteAsync(user).Result;
            var startDate = new DateTime(2018, 06, 14);
            if (DateTime.Now > startDate) _statsService.UpdateUserPointsAndPositions(startDate);
        }

        public IEnumerable<ApplicationUser> GetUsers() => _userManager.Users;
    }
}
