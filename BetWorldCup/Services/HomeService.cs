﻿using System;
using System.Collections.Generic;
using System.Linq;
using BetWorldCup.Helpers;
using BetWorldCup.Models;
using BetWorldCup.Models.BetViewModels;
using BetWorldCup.Services.Repository;
using Microsoft.AspNetCore.Identity;

namespace BetWorldCup.Services
{
    public class HomeService : IHomeService
    {
        private readonly IMatchRepository _matchRepository;
        private readonly IBetRepository _betRepository;
        private readonly IRepository<ResultType> _resultRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public HomeService(
            IMatchRepository matchRepository, 
            IBetRepository betRepository, 
            IRepository<ResultType> resultRepository,
            UserManager<ApplicationUser> userManager)
        {
            _matchRepository = matchRepository;
            _betRepository = betRepository;
            _resultRepository = resultRepository;
            _userManager = userManager;
        }

        public IEnumerable<Match> GetMatchesForStartScreen()
        {
            return _matchRepository.GetMatchesForStartScreen(true);
        }

        public IEnumerable<Match> GetAllMatches()
        {
            return _matchRepository.GetAllMatches(true);
        }

        public Match GetSingleMatch(int id)
        {
            return _matchRepository.GetSingleMatch(true, id);
        }

        public Bet PlaceBet(Bet model, string email, int? matchId)
        {
            var match = _matchRepository.GetById(matchId);
            if (model.BetId == 0) model.Match = match;

            if (match.StartDate < DateTime.Now)
            {
                throw new Exception("Czas obstawiania tego meczu minął!");
            }

            model.Result = _resultRepository.GetById(BetHelpers.GetResultType(model.HomeScore, model.AwayScore));
            model.User = _userManager.FindByEmailAsync(email).Result;

            return _betRepository.InsertOrUpdate(model);
        }

        public string[] GetUserNames() => _userManager.Users.Select(p => p.Name).ToArray();
        public string[] GetMatchIds() => _matchRepository.GetAll(true).Select(p => p.MatchId.ToString()).ToArray();
    }
}
