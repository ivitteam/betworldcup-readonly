﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BetWorldCup.Services.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Match = BetWorldCup.Models.BetViewModels.Match;

namespace BetWorldCup.Services.RESTApi
{
    internal class ScoreUpdater : IHostedService, IDisposable
    {
        private Timer _timer;

        private IAdminService _adminService;
        private IStatsService _statsService;
        private ILiveScoresService _liveScoresService;
        private IMatchRepository _matchRepository;
        private ILogger<ScoreUpdater> _logger;

        private readonly IServiceProvider _serviceProvider;
        
        public ScoreUpdater(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(120));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            try
            {
                IEnumerable<Match> unfinishedMatches;

                using (var scope = _serviceProvider.CreateScope())
                {
                    _matchRepository = scope.ServiceProvider.GetRequiredService<IMatchRepository>();
                    unfinishedMatches = _matchRepository.GetUnfinishedMatches(true).ToArray();
                }

                if (!unfinishedMatches.Any()) return;
                var changedMatches = false;

                foreach (var match in unfinishedMatches)
                {
                    Models.RESTApi.Match liveScore;
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var tokenSource = new CancellationTokenSource();
                        _liveScoresService = scope.ServiceProvider.GetRequiredService<ILiveScoresService>();

                        var liveMatches = _liveScoresService.GetForLeague(tokenSource.Token, match.LeagueId).Result;
                        liveScore = liveMatches.data.match.SingleOrDefault(p => 
                            p.home_name == match.HomeTeam.APIName && p.away_name == match.AwayTeam.APIName 
                            || p.home_id == match.HomeTeam.APIId && p.away_id == match.AwayTeam.APIId);
                    }

                    if (liveScore == null) continue;

                    if (!string.IsNullOrEmpty(liveScore.ft_score))
                    {
                        var score = GetIntScore(liveScore.ft_score);
                        match.HomeScore = score[0];
                        match.AwayScore = score[1];
                        match.IsFinished = true;
                        changedMatches = true;
                        using (var scope = _serviceProvider.CreateScope())
                        {
                            _adminService = scope.ServiceProvider.GetRequiredService<IAdminService>();
                            _adminService.UpdateMatchScore(match);
                        }
                    }
                    else if ($"{match.HomeScore} - {match.AwayScore}" != liveScore.score 
                             || match.Result == null && liveScore.score != "")
                    {
                        var score = GetIntScore(liveScore.score);
                        match.HomeScore = score[0];
                        match.AwayScore = score[1];
                        changedMatches = true;
                        using (var scope = _serviceProvider.CreateScope())
                        {
                            _adminService = scope.ServiceProvider.GetRequiredService<IAdminService>();
                            _adminService.UpdateMatchScore(match);
                        }
                    }

                }

                if (!changedMatches) return;

                using (var scope = _serviceProvider.CreateScope())
                {
                    _statsService = scope.ServiceProvider.GetRequiredService<IStatsService>();
                    _statsService.UpdateUserPointsAndPositions(unfinishedMatches.OrderBy(p => p.StartDate).First().StartDate);
                }
            }
            catch (Exception e)
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    _logger = scope.ServiceProvider.GetRequiredService<ILogger<ScoreUpdater>>();
                    _logger.LogError(e, "BACKGROUND WORKER");
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        private static List<int> GetIntScore(string score)
        {
            var scoreArray = score?.Split(" - ");
            return new List<int> {int.Parse(scoreArray?[0] ?? "0"), int.Parse(scoreArray?[1] ?? "0")};
        }
    }
}
