﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using BetWorldCup.Models.RESTApi.Table;
using Newtonsoft.Json;

namespace BetWorldCup.Services.RESTApi
{
    public class GroupTableService : IGroupTableService
    {
        private const string API_KEY = "hzm6mKrA4E5BX2EI";
        private const string API_SECRET = "KIyl7C3ovUU9Rk19Toff8HQdlWOjYs1J";
        private readonly HttpClient _client;

        public GroupTableService()
        {
            _client = new HttpClient {BaseAddress = new Uri(@"http://livescore-api.com/api-client/")};
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<RootObject> GetTable(int leagueId)
        {
            var response = await _client.GetAsync(GetUriString(leagueId), new CancellationToken());
            if (!response.IsSuccessStatusCode) return new RootObject();
            var stringResult = await response.Content.ReadAsStringAsync();
            return TableFromJson(stringResult);
        }

        private static RootObject TableFromJson(string json)
        {
            return JsonConvert.DeserializeObject<RootObject>(json);
        }

        private static string GetUriString(int leagueId)
        {
            return $"http://livescore-api.com/api-client/leagues/table.json?key={API_KEY}&secret={API_SECRET}&league={leagueId}";
        }
    }
}
