﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using BetWorldCup.Models.RESTApi;
using Newtonsoft.Json;

namespace BetWorldCup.Services.RESTApi
{
    public class LiveScoresService : ILiveScoresService
    {
        private const string API_KEY = "hzm6mKrA4E5BX2EI";
        private const string API_SECRET = "KIyl7C3ovUU9Rk19Toff8HQdlWOjYs1J";
        private readonly HttpClient _client;

        public LiveScoresService()
        {
            _client = new HttpClient {BaseAddress = new Uri(@"http://livescore-api.com/api-client/")};
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<Response> GetForLeague(CancellationToken cancellationToken, int leagueId)
        {
            if (leagueId == 0)
            {
                return await GetAll(cancellationToken);
            }

            var response = await _client.GetAsync(GetUriString(leagueId), cancellationToken);
            if (!response.IsSuccessStatusCode) return new Response();
            var stringResult = await response.Content.ReadAsStringAsync();
            return ScoresFromJson(stringResult);
        }

        public async Task<Response> GetAll(CancellationToken cancellationToken)
        {
            var response = await _client.GetAsync(GetUriString(null), cancellationToken);
            if (!response.IsSuccessStatusCode) return new Response();
            var stringResult = await response.Content.ReadAsStringAsync();
            return ScoresFromJson(stringResult);
        }

        private static Response ScoresFromJson(string json)
        {
            return JsonConvert.DeserializeObject<Response>(json);
        }

        private static string GetUriString(int? leagueId)
        {
            return leagueId == null ? $"http://livescore-api.com/api-client/scores/live.json?key={API_KEY}&secret={API_SECRET}" : $"http://livescore-api.com/api-client/scores/live.json?key={API_KEY}&secret={API_SECRET}&league={leagueId}";
        }
    }
}
