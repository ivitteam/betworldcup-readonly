﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BetWorldCup.Models;
using BetWorldCup.Models.BetViewModels;
using BetWorldCup.Models.RESTApi.Table;
using BetWorldCup.Models.StatsViewModels;
using BetWorldCup.Services.Repository;
using BetWorldCup.Services.RESTApi;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BetWorldCup.Services
{
    public class StatsService : IStatsService
    {
        private readonly IMatchRepository _matchRepository;
        private readonly IBetRepository _betRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IRepository<BetResult> _betResultRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IStandingsRepository _standingsRepository;
        private readonly IGroupTableService _groupTableService;

        public StatsService(
            IMatchRepository matchRepository,
            UserManager<ApplicationUser> userManager,
            IRepository<BetResult> betResultRepository,
            IBetRepository betRepository,
            IStandingsRepository standingsRepository,
            IRepository<Team> teamRepository,
            IGroupTableService groupTableService)
        {
            _matchRepository = matchRepository;
            _userManager = userManager;
            _betResultRepository = betResultRepository;
            _betRepository = betRepository;
            _standingsRepository = standingsRepository;
            _teamRepository = teamRepository;
            _groupTableService = groupTableService;
        }

        #region Regular Data

        public Team GetTeam(int teamId) => _teamRepository.GetById(teamId);

        public IEnumerable<Match> GetTeamMatches(int teamId) => _matchRepository.GetAllMatches(true)
            .Where(p => p.HomeTeam.TeamId == teamId || p.AwayTeam.TeamId == teamId);

        #endregion

        #region PIE Charts

        public List<object> PieChartUserPointsRatio(int matchId)
        {
            #region Pobranie danych

            var sumUsers = _userManager.Users.AsNoTracking().Count();
            var bets = _matchRepository.GetSingleMatch(true, matchId).Bets;

            var scoreHit = bets.Count(p => p.BetResult?.Type == "SCORE_MATCH");
            var resultHit = bets.Count(p => p.BetResult?.Type == "RESULT_MATCH");
            var noHit = bets.Count(p => p.BetResult?.Type == "NO_MATCH");
            var noBet = sumUsers - noHit - scoreHit - resultHit;

            var betResults = _betResultRepository.GetAll(true).ToArray();

            #endregion

            var data = new List<object>();
            var dt = new DataTable();

            dt.Columns.Add("betPoints");
            dt.Columns.Add("count");
            dt.Columns.Add("colors");

            var dataRow = dt.NewRow();
            dataRow["betPoints"] = betResults.Single(p => p.Type == "SCORE_MATCH").TypeToString;
            dataRow["count"] = scoreHit;
            dataRow["colors"] = "#3BC36F";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betPoints"] = betResults.Single(p => p.Type == "RESULT_MATCH").TypeToString;
            dataRow["count"] = resultHit;
            dataRow["colors"] = "#71A0DE";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betPoints"] = betResults.Single(p => p.Type == "NO_MATCH").TypeToString;
            dataRow["count"] = noHit;
            dataRow["colors"] = "#EB6363";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betPoints"] = "Nie typowali";
            dataRow["count"] = noBet;
            dataRow["colors"] = "#D3D3D3";
            dt.Rows.Add(dataRow);

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        public List<object> PieChartOverUnderRatio(int matchId)
        {
            #region Pobranie danych

            var match = _matchRepository.GetSingleMatch(true, matchId);

            var overCount = match.Bets.Count(p => p.HomeScore + p.AwayScore > 2);
            var underCount = match.Bets.Count - overCount;

            #endregion

            var data = new List<object>();
            var dt = new DataTable();

            dt.Columns.Add("betResult");
            dt.Columns.Add("count");
            dt.Columns.Add("colors");

            var dataRow = dt.NewRow();
            dataRow["betResult"] = "Powyżej";
            dataRow["count"] = overCount;
            dataRow["colors"] = "#3BC36F";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betResult"] = "Poniżej";
            dataRow["count"] = underCount;
            dataRow["colors"] = "#EB6363";
            dt.Rows.Add(dataRow);

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        public List<object> PieChartMatchResultRatio(int matchId)
        {
            #region Pobranie danych

            var match = _matchRepository.GetSingleMatch(true, matchId);

            var drawsCount = match.Bets.Count(p => p.Result.Type == "DRAW");
            var homeCount = match.Bets.Count(p => p.Result.Type == "WIN");
            var awayCount = match.Bets.Count(p => p.Result.Type == "LOSE");

            #endregion

            var data = new List<object>();
            var dt = new DataTable();

            dt.Columns.Add("betResult");
            dt.Columns.Add("count");
            dt.Columns.Add("colors");

            var dataRow = dt.NewRow();
            dataRow["betResult"] = "Remis";
            dataRow["count"] = drawsCount;
            dataRow["colors"] = "#71A0DE";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betResult"] = match.HomeTeam.LongName;
            dataRow["count"] = homeCount;
            dataRow["colors"] = "#3BC36F";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betResult"] = match.AwayTeam.LongName;
            dataRow["count"] = awayCount;
            dataRow["colors"] = "#EB6363";
            dt.Rows.Add(dataRow);

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;

        }

        public List<object> PieChartOverUnderRatioAllScores()
        {
            #region Pobranie danych

            var bets = _betRepository.GetBetsOfStartedMatches(true).ToArray();
            
            var overCount = bets.Count(p => p.HomeScore + p.AwayScore > 2);
            var underCount = bets.Length - overCount;

            #endregion

            var data = new List<object>();
            var dt = new DataTable();

            dt.Columns.Add("betResult");
            dt.Columns.Add("count");
            dt.Columns.Add("colors");

            var dataRow = dt.NewRow();
            dataRow["betResult"] = "Powyżej";
            dataRow["count"] = overCount;
            dataRow["colors"] = "#3BC36F";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betResult"] = "Poniżej";
            dataRow["count"] = underCount;
            dataRow["colors"] = "#EB6363";
            dt.Rows.Add(dataRow);

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        public List<object> PieChartUserPointsRatioAllMatch()
        {
            #region Pobranie danych
            var users = _userManager.Users.Include(p => p.Bets).ThenInclude(p => p.BetResult).ToArray();
            var bets = users.SelectMany(p => p.Bets).ToList();

            var scoreHits = bets.Count(p => p.BetResult?.Type == "SCORE_MATCH");
            var resultHit = bets.Count(p => p.BetResult?.Type == "RESULT_MATCH");
            var noHit = bets.Count(p => p.BetResult?.Type == "NO_MATCH");
            var noBet = _matchRepository.GetAll(true).Count(p => p.StartDate < DateTime.Now) * users.Length - scoreHits - resultHit - noHit;

            var betResults = _betResultRepository.GetAll(true).ToArray();

            #endregion

            var data = new List<object>();
            var dt = new DataTable();

            dt.Columns.Add("betPoints");
            dt.Columns.Add("count");
            dt.Columns.Add("colors");

            var dataRow = dt.NewRow();
            dataRow["betPoints"] = betResults.Single(p => p.Type == "SCORE_MATCH").TypeToString;
            dataRow["count"] = scoreHits;
            dataRow["colors"] = "#3BC36F";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betPoints"] = betResults.Single(p => p.Type == "RESULT_MATCH").TypeToString;
            dataRow["count"] = resultHit;
            dataRow["colors"] = "#71A0DE";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betPoints"] = betResults.Single(p => p.Type == "NO_MATCH").TypeToString;
            dataRow["count"] = noHit;
            dataRow["colors"] = "#EB6363";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betPoints"] = "Nie typowali";
            dataRow["count"] = noBet;
            dataRow["colors"] = "#D3D3D3";
            dt.Rows.Add(dataRow);

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        public List<object> PieChartTeamAvarageGoals(int teamId)
        {
            #region Pobranie danych

            var matches = _matchRepository.GetAllMatches(true).Where(p => (p.HomeTeam.TeamId == teamId || p.AwayTeam.TeamId == teamId) && p.IsFinished).ToList();
            var goalsScored = matches.Sum(p => p.HomeTeam.TeamId == teamId ? p.HomeScore : p.AwayScore);
            var goalsConceded = matches.Sum(p => p.HomeTeam.TeamId == teamId ? p.AwayScore : p.HomeScore);
            #endregion

            var data = new List<object>();
            var dt = new DataTable();

            dt.Columns.Add("label");
            dt.Columns.Add("count");
            dt.Columns.Add("colors");

            var dataRow = dt.NewRow();
            dataRow["label"] = "Strzelone";
            dataRow["count"] = ((double)goalsScored / matches.Count).ToString("F2").Replace(",",".");
            dataRow["colors"] = "#3BC36F";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["label"] = "Stracone";
            dataRow["count"] = ((double)goalsConceded / matches.Count).ToString("F2").Replace(",",".");
            dataRow["colors"] = "#EB6363";
            dt.Rows.Add(dataRow);

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        public List<object> PieChartTeamResults(int teamId)
        {
            #region Pobranie danych

            var matches = _matchRepository.GetAllMatches(true).Where(p => (p.HomeTeam.TeamId == teamId || p.AwayTeam.TeamId == teamId) && p.IsFinished).ToList();

            var drawCount = matches.Count(p => p.Result.Type == "DRAW");
            var winCount = matches.Count(p => p.Result.Type == "WIN" && p.HomeTeam.TeamId == teamId || p.Result.Type == "LOSE" && p.AwayTeam.TeamId == teamId);
            var loseCount = matches.Count(p => p.Result.Type == "LOSE" && p.HomeTeam.TeamId == teamId || p.Result.Type == "WIN" && p.AwayTeam.TeamId == teamId);

            #endregion

            var data = new List<object>();
            var dt = new DataTable();

            dt.Columns.Add("betResult");
            dt.Columns.Add("count");
            dt.Columns.Add("colors");

            var dataRow = dt.NewRow();
            dataRow["betResult"] = "Remis";
            dataRow["count"] = drawCount;
            dataRow["colors"] = "#71A0DE";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betResult"] = "Zwycięstwo";
            dataRow["count"] = winCount;
            dataRow["colors"] = "#3BC36F";
            dt.Rows.Add(dataRow);

            dataRow = dt.NewRow();
            dataRow["betResult"] = "Porażka";
            dataRow["count"] = loseCount;
            dataRow["colors"] = "#EB6363";
            dt.Rows.Add(dataRow);

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }
        #endregion

        #region BAR Charts

        public List<object> BarChartMatchScoreRatio(int matchId)
        {
            #region Pobranie danych

            var match = _matchRepository.GetSingleMatch(true, matchId);
            var scoreList = match.Bets.Select(p => $"{p.HomeScore} : {p.AwayScore}").OrderBy(p => p).ToArray();

            #endregion

            var data = new List<object>();
            var dt = new DataTable();

            dt.Columns.Add("betScore");
            dt.Columns.Add("count");

            foreach (var score in scoreList.Distinct())
            {
                var dataRow = dt.NewRow();
                dataRow["betScore"] = score;
                dataRow["count"] = scoreList.Count(p => p == score);
                dt.Rows.Add(dataRow);
            }

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        public List<object> BarChartUserBetsPoints(string userName)
        {
            #region Pobranie danych

            var bets = _userManager.Users
                .Include(p => p.Bets).ThenInclude(p => p.Match).ThenInclude(p => p.HomeTeam)
                .Include(p => p.Bets).ThenInclude(p => p.Match).ThenInclude(p => p.AwayTeam)
                .Include(p => p.Bets).ThenInclude(p => p.BetResult)
                .Single(p => p.UserName == userName)
                .Bets.Where(p => p.Match.StartDate < DateTime.Now)
                .OrderByDescending(p => p.Match.StartDate).ToArray();
            #endregion

            var data = new List<object>();
            var dt = new DataTable();

            dt.Columns.Add("match");
            dt.Columns.Add("points");
            dt.Columns.Add("score");

            foreach (var bet in bets)
            {
                var dataRow = dt.NewRow();
                dataRow["match"] = $"{bet.Match.HomeTeam.ShortName}-{bet.Match.AwayTeam.ShortName}";
                dataRow["points"] = bet.BetResult?.Points * bet.Match.Multiplier * bet.Match.HomeTeam.Multiplier * bet.Match.AwayTeam.Multiplier ?? 0;
                dataRow["score"] = $"{bet.HomeScore} - {bet.AwayScore}";

                dt.Rows.Add(dataRow);
            }

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        #endregion

        #region LINE Charts

        public List<object> LineGetPointsHistoryForUser(string userName)
         {

            #region Pobieranie danych

            var userStandings = _userManager.Users.Include(p => p.UserStandings).Single(p => p.UserName == userName).UserStandings;
            var dates = GetPlayedMatchesDays();

            #endregion

            var data = new List<object>();
            var dt = new DataTable();

            dt.Columns.Add("dates");
            dt.Columns.Add("points");

            

            foreach (var date in dates)
            {
                var stats = userStandings.SingleOrDefault(p => p.DateFor.Date == date.Date);
                if (stats == null) continue;
                var dataRow = dt.NewRow();
                dataRow["dates"] = date.ToString("dd MMM");
                dataRow["points"] = stats.Points;
                dt.Rows.Add(dataRow);
            }

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }

        public List<object> LineGetPositionHistoryForUser(string userName)
        {
            #region Pobieranie danych

            var userStandings = _userManager.Users.Include(p => p.UserStandings).Single(p => p.UserName == userName).UserStandings;
            var dates = GetPlayedMatchesDays();

            #endregion

            var data = new List<object>();
            var dt = new DataTable();

            dt.Columns.Add("dates");
            dt.Columns.Add("position");

            foreach (var date in dates)
            {
                var stats = userStandings.SingleOrDefault(p => p.DateFor.Date == date.Date);
                if (stats == null) continue;
                var dataRow = dt.NewRow();
                dataRow["dates"] = date.ToString("dd MMM");
                dataRow["position"] = stats.Position;
                dt.Rows.Add(dataRow);
            }

            foreach (DataColumn dataCol in dt.Columns)
            {
                var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                data.Add(column);
            }

            return data;
        }
        
        public List<object> LineGetPositionHistoryForAllUser(string userName)
        {

            #region Pobieranie danych

            var userStandings = _userManager.Users.Include(p => p.UserStandings).OrderByDescending(a=>a.Points).Take(5).ToList();
            var userStandingsLoggedUser = _userManager.Users.Include(p => p.UserStandings).Single(p => p.UserName == userName);
            if (userStandings.All(p => p.Id != userStandingsLoggedUser.Id)) userStandings.Add(userStandingsLoggedUser);

            var dates = GetPlayedMatchesDays().ToArray();

            #endregion

            var dataResult = new List<object>();
            
            foreach (var usr in userStandings.Select(a=>a.UserStandings))
            {
                var data = new List<object>();
                var dt = new DataTable();

                dt.Columns.Add("dates");
                dt.Columns.Add("position");
                dt.Columns.Add("name");

                if (dates.Length == 0)
                {
                    var dataRow = dt.NewRow();
                    dataRow["dates"] = DateTime.Now.ToString("dd MMM");
                    dataRow["position"] = 999;
                    dataRow["name"] = "n/d";

                    dt.Rows.Add(dataRow);
                }

                foreach (var date in dates)
                {
                    var stats = usr.SingleOrDefault(p => p.DateFor.Date == date.Date);
                    if (stats == null) continue;
                    var dataRow = dt.NewRow();
                    dataRow["dates"] = date.ToString("dd MMM");
                    dataRow["position"] = stats.Position;
                    dataRow["name"] = stats.User.Name;
                    dt.Rows.Add(dataRow);
                }
                foreach (DataColumn dataCol in dt.Columns)
                {
                    var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                    data.Add(column);
                }
                 dataResult.Add(data);

            }
 
            return dataResult;
        }
        
        public List<object> LineGetPointsHistoryForAllUser(string userName)
        {

            #region Pobieranie danych

            var userStandings = _userManager.Users.Include(p => p.UserStandings).OrderByDescending(a=>a.Points).Take(5).ToList();
            var userStandingsLoggedUser = _userManager.Users.Include(p => p.UserStandings).Single(p => p.UserName == userName);
            if (userStandings.All(p => p.Id != userStandingsLoggedUser.Id)) userStandings.Add(userStandingsLoggedUser);
            var dates = GetPlayedMatchesDays().ToArray();

            #endregion

            var dataResult = new List<object>();

            
            foreach (var usr in userStandings.Select(a=>a.UserStandings))
            {
                var data = new List<object>();
                var dt = new DataTable();

                dt.Columns.Add("dates");
                dt.Columns.Add("points");
                dt.Columns.Add("name");
                

                if (dates.Length == 0)
                {
                    var dataRow = dt.NewRow();
                    dataRow["dates"] = DateTime.Now.ToString("dd MMM");
                    dataRow["points"] = 0;
                    dataRow["name"] = "n/d";

                    dt.Rows.Add(dataRow);
                }

                foreach (var date in dates)
                {
                    var stats = usr.SingleOrDefault(p => p.DateFor.Date == date.Date);
                    if (stats == null) continue;
                    var dataRow = dt.NewRow();
                    dataRow["dates"] = date.ToString("dd MMM");
                    dataRow["points"] = stats.Points;
                    dataRow["name"] = stats.User.Name;

                    dt.Rows.Add(dataRow);
                }
                foreach (DataColumn dataCol in dt.Columns)
                {
                    var column = (from DataRow dr in dt.Rows select dr[dataCol.ColumnName]).ToList();
                    data.Add(column);
                }
                 dataResult.Add(data);

            }
         
            return dataResult;
        }
        #endregion

        #region TABLE Data

        private IEnumerable<ClassificationRow> GetFullClassification(DateTime date)
        {
            var result = new List<ClassificationRow>();
            foreach (var user in _userManager.Users)
            {
                result.Add(new ClassificationRow { Points = GetUserPoints(user, date), Name = user.Name, Id = user.Id });
            }
            result = result.OrderByDescending(p => p.Points).ToList();

            for (var i = 0; i < result.Count; i++)
            {
                if (result.Count(p => p.Points == result[i].Points) == 1)
                {
                    result[i].Position = i + 1;
                }
                else
                {
                    var list = result.Where(p => p.Points == result[i].Points).ToList();
                    var sortedList = SortRows(list, date);

                    foreach (var row in sortedList)
                    {
                        row.Position += i;
                    }

                    i += sortedList.Count - 1;
                }
            }



            return result.OrderBy(p => p.Position);
        }

        public IEnumerable<ClassificationRow> GetActuallFullClassification(string currentUserName)
        {
            var result = new List<ClassificationRow>();
            foreach (var user in _userManager.Users)
            {
                result.Add(new ClassificationRow { Points = user.Points, Name = user.Name, Id = user.Id, Position = user.Position, IsLoggedUser = user.UserName == currentUserName, IsFunUser =  user.FunAccount});
            }
            return result.OrderBy(p => p.Position);
        }

        public IEnumerable<ClassificationRow> GetAllUserScoreHits()
        {
              var users = _userManager.Users.Include(p => p.Bets).ThenInclude(p => p.BetResult).ToArray();
           
            var lista=new List<ClassificationRow>();
            foreach (var zmienna in users)
            {
                var ile = zmienna.Bets.Count(p => p.BetResult?.Type == "SCORE_MATCH");
                var model = new ClassificationRow
                {
                    Points = ile,
                    Name = zmienna.Name
                };
                lista.Add(model);
            }
     
            var result = lista.OrderByDescending(a => a.Points).Take(5);
           
            return result;
        }

        public IEnumerable<ClassificationRow> GetAllUserResulHits()
        {
            var users = _userManager.Users.Include(p => p.Bets).ThenInclude(p => p.BetResult).ToArray();

            var lista = new List<ClassificationRow>();
            foreach (var zmienna in users)
            {
                var ile = zmienna.Bets.Count(p => p.BetResult?.Type == "RESULT_MATCH");
                var model = new ClassificationRow
                {
                    Points = ile,
                    Name = zmienna.Name
                };
                lista.Add(model);
            }

            var result = lista.OrderByDescending(a => a.Points).Take(5);
            return result;
        }

        public IEnumerable<ClassificationRow> GetAllUserMisses()
        {
        
            var users = _userManager.Users.Include(p => p.Bets).ThenInclude(p => p.BetResult).ToArray();

            var lista = new List<ClassificationRow>();
            foreach (var zmienna in users)
            {
                var ile = zmienna.Bets.Count(p => p.BetResult?.Type == "NO_MATCH");
                var model = new ClassificationRow
                {
                    Points = ile,
                    Name = zmienna.Name
                };
                lista.Add(model);
            }
            var result = lista.OrderByDescending(a => a.Points).Take(5);
            return result;
        }
        
        public IEnumerable<ClassificationRow> GetBestMatches()
        {
            var matches = _matchRepository.GetAllMatches(true).Where(p => p.StartDate < DateTime.Now)
                .OrderByDescending(p => p.Bets.Sum(c => c.BetResult?.Points)).Take(5);

            var lista = new List<ClassificationRow>();
            foreach (var match in matches)
            {
                var model = new ClassificationRow
                {
                    Id = $"{match.HomeTeam.ShortName} - {match.AwayTeam.ShortName}",
                    Name = $"<b>{match.Bets.Sum(p => p.BetResult?.Points)} pkt.</b> - {match.Bets.Count(p => p.BetResult?.Type == "SCORE_MATCH")}/{match.Bets.Count(p => p.BetResult?.Type == "RESULT_MATCH")}/{match.Bets.Count(p => p.BetResult?.Type == "NO_MATCH")}",
                    Position = match.MatchId
                };
                lista.Add(model);
            }

            return lista;
        }
        
        public IEnumerable<ClassificationRow> GetWorstMatches()
        {
            var matches = _matchRepository.GetAllMatches(true).Where(p => p.StartDate < DateTime.Now)
                .OrderBy(p => p.Bets.Sum(c => c.BetResult?.Points)).Take(5);

            var lista = new List<ClassificationRow>();
            foreach (var match in matches)
            {
                var model = new ClassificationRow
                {
                    Id = $"{match.HomeTeam.ShortName} - {match.AwayTeam.ShortName}",
                    Name = $"<b>{match.Bets.Sum(p => p.BetResult?.Points)} pkt.</b> - {match.Bets.Count(p => p.BetResult?.Type == "SCORE_MATCH")}/{match.Bets.Count(p => p.BetResult?.Type == "RESULT_MATCH")}/{match.Bets.Count(p => p.BetResult?.Type == "NO_MATCH")}",
                    Position = match.MatchId
                };
                lista.Add(model);
            }

            return lista;
        }

        public IEnumerable<ClassificationRow> GetBestTeams()
        {
            var matches = _matchRepository.GetAllMatches(true).Where(p => p.StartDate < DateTime.Now).ToList();
            var teams = matches.Select(p => p.HomeTeam.LongName).ToList();
            teams.AddRange(matches.Select(p => p.AwayTeam.LongName).ToList());
            
            var lista = new List<ClassificationRow>();
            foreach (var team in teams.Distinct())
            {
                var tempPoints = 0;
                var matchesWithTeam = matches.Where(p => p.HomeTeam.LongName == team || p.AwayTeam.LongName == team).ToArray();

                foreach (var match in matchesWithTeam)
                {
                    var matchPoints = match.Bets.Sum(p => p.BetResult?.Points);
                    tempPoints += matchPoints ?? 0;
                }

                if (matchesWithTeam.Length <= 0) continue;
                
                var model = new ClassificationRow
                {
                    Id = team,
                    Name = $"{tempPoints / matchesWithTeam.Length:D} pkt.",
                    Points = _teamRepository.GetAll(true).Single(p => p.LongName == team).TeamId,
                    Position = tempPoints / matchesWithTeam.Length
                };
                lista.Add(model);
            }

            return lista.OrderByDescending(p => p.Position).Take(5);
        }

        public IEnumerable<ClassificationRow> GetWorstTeams()
        {
            var matches = _matchRepository.GetAllMatches(true).Where(p => p.StartDate < DateTime.Now).ToList();
            var teams = matches.Select(p => p.HomeTeam.LongName).ToList();
            teams.AddRange(matches.Select(p => p.AwayTeam.LongName).ToList());
            
            var lista = new List<ClassificationRow>();
            foreach (var team in teams.Distinct())
            {
                int tempPoints = 0;
                var matchesWithTeam = matches.Where(p => p.HomeTeam.LongName == team || p.AwayTeam.LongName == team).ToArray();

                foreach (var match in matchesWithTeam)
                {
                    var matchPoints = match.Bets.Sum(p => p.BetResult?.Points);
                    tempPoints += matchPoints ?? 0;
                }

                if (matchesWithTeam.Length <= 0) continue;
                
                var model = new ClassificationRow
                {
                    Id = team,
                    Name = $"{tempPoints / matchesWithTeam.Length:D} pkt.",
                    Points = _teamRepository.GetAll(true).Single(p => p.LongName == team).TeamId,
                    Position = tempPoints / matchesWithTeam.Length
                };
                lista.Add(model);
            }

            return lista.OrderBy(p => p.Position).Take(5);
        }

        public IEnumerable<Table> GetTable(int leagueId)
        {
            var result = _groupTableService.GetTable(leagueId).Result.data.table;
            var teams = _teamRepository.GetAll(true).ToList();

            foreach (var row in result)
            {
                var team = teams.SingleOrDefault(p => p.APIName == row.name.Replace("Korea Republic", "South Korea").Replace("Morocco", "Morroco"));
                if (team == null) continue;

                row.LongName = team.LongName;
                row.ShortName = team.ShortName;
                row.TeamId = team.TeamId;
            }

            return result;
        } 

        #endregion

        #region USER Data

        public IEnumerable<UserMatchResult> GetUserMatchResults(int matchId, string currentUserName)
        {
            var match = _matchRepository.GetSingleMatch(true, matchId);
            var multiplier = match.Multiplier * match.HomeTeam.Multiplier * match.AwayTeam.Multiplier;
            var result = new List<UserMatchResult>();

            // Nie można zamienić na Lamba expresion ze względu na null-propagation
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var user in _userManager.Users.Include(p => p.Bets).ThenInclude(p => p.Match).Include(p => p.Bets).ThenInclude(p => p.BetResult))
            {
                var bet = user.Bets.FirstOrDefault(p => p.Match.MatchId == matchId);
                var points = bet?.BetResult?.Points * multiplier ?? 0;
                var betResult = bet?.BetResult?.TypeToString ?? "Brak typu";
                var betScore = bet == null ? "Brak typu" : $"{bet.HomeScore} : {bet.AwayScore}";

                result.Add(new UserMatchResult { Points = points, Result = betResult, Username = user.Name, Score = betScore, IsFunUser = user.FunAccount, IsLoggedUser = user.UserName == currentUserName});
            }

            return result.OrderByDescending(p => p.Points);
        }

        public int GetUserScoreHits(ApplicationUser user, DateTime date) => user.Bets.Where(p => p.Match.StartDate < date).Count(p => p.BetResult?.Type == "SCORE_MATCH");
        public int GetUserResultHits(ApplicationUser user, DateTime date) => user.Bets.Where(p => p.Match.StartDate < date).Count(p => p.BetResult?.Type == "RESULT_MATCH");
        public int GetUserMisses(ApplicationUser user, DateTime date) => user.Bets.Where(p => p.Match.StartDate < date).Count(p => p.BetResult?.Type == "NO_MATCH");
        public int GetUserNoBets(ApplicationUser user, DateTime date) => _matchRepository.GetAll(true).Count(p => p.StartDate < date) - user.Bets.Count(p => p.Match.StartDate < date);
        private int GetUserPoints(ApplicationUser user, DateTime date)
        {
            return _betRepository.GetUserBets(true, user.Id).Where(p => p.Match.StartDate < date).Select(p =>
                    p.BetResult?.Points * p.Match.Multiplier * p.Match.HomeTeam.Multiplier * p.Match.AwayTeam.Multiplier ?? 0 )
                .Sum();
        }
        public ApplicationUser GetUserByName(string userName) => _userManager.Users
            .Include(p => p.Bets).ThenInclude(p => p.Match)
            .Include(p => p.Bets).ThenInclude(p => p.BetResult)
            .Include(p => p.Bets).ThenInclude(p => p.Result)
            .AsNoTracking()
            .SingleOrDefault(p => p.Name == userName);
 
        #endregion

        #region UPDATE's methods

        public void UpdateUserPointsAndPositions(DateTime date)
        {
            var fullClassification = GetFullClassification(DateTime.Now).ToArray();
            var daysLaterThanMatchDay = GetPlayedMatchesDays().Where(p => p.Date > date.Date).ToList();

            if (!daysLaterThanMatchDay.Any())
            {
                foreach (var user in _userManager.Users)
                {
                    user.Points = fullClassification.SingleOrDefault(p => p.Name == user.Name)?.Points ?? 0;
                    user.Position = fullClassification.SingleOrDefault(p => p.Name == user.Name)?.Position ?? 9999;
                    var unused = _userManager.UpdateAsync(user).Result;

                    _standingsRepository.InsertOrUpdate(new UserStandings
                    {
                        User = user,
                        Points = user.Points,
                        Position = user.Position,
                        DateFor = date.Date,
                        Id = $"{user.Id}|{date.Date:yyyy-MM-dd}"
                    });
                }
            }
            else
            {
                foreach (var user in _userManager.Users)
                {
                    user.Points = fullClassification.SingleOrDefault(p => p.Name == user.Name)?.Points ?? 0;
                    user.Position = fullClassification.SingleOrDefault(p => p.Name == user.Name)?.Position ?? 9999;
                    var unused = _userManager.UpdateAsync(user).Result;
                }

                daysLaterThanMatchDay.Add(date.Date);
                foreach (var dateLater in daysLaterThanMatchDay)
                {
                    var classification = GetFullClassification(dateLater.AddDays(1).AddSeconds(-1)).ToArray();
                    foreach (var user in _userManager.Users)
                    {
                        _standingsRepository.InsertOrUpdate(new UserStandings
                        {
                            User = user,
                            Points = classification.SingleOrDefault(p => p.Name == user.Name)?.Points ?? 0,
                            Position = classification.SingleOrDefault(p => p.Name == user.Name)?.Position ?? 9999,
                            DateFor = dateLater.Date,
                            Id = $"{user.Id}|{dateLater.Date:yyyy-MM-dd}"
                        });
                    }

                }
            }
        }

        public void UpdateUserPointsAndPositionsForSeed(DateTime date)
        {
            var fullClassification = GetFullClassification(date).ToArray();
            
            foreach (var user in _userManager.Users)
            {
                user.Points = fullClassification.SingleOrDefault(p => p.Name == user.Name)?.Points ?? 0;
                user.Position = fullClassification.SingleOrDefault(p => p.Name == user.Name)?.Position ?? 9999;
                var unused = _userManager.UpdateAsync(user).Result;

                _standingsRepository.InsertOrUpdate(new UserStandings
                {
                    User = user,
                    Points = user.Points,
                    Position = user.Position,
                    DateFor = date.Date,
                    Id = $"{user.Id}|{date.Date:yyyy-MM-dd}"
                });
            }
        }

        #endregion

        #region HELPERS

        private List<ClassificationRow> SortRows(IEnumerable<ClassificationRow> rows, DateTime date)
        {
            var result = new List<ClassificationRow>();

            foreach (var row in rows)
            {
                var userBets = _userManager.Users
                    .Include(p => p.Bets).ThenInclude(p => p.Match)
                    .Include(p => p.Bets).ThenInclude(p => p.BetResult)
                    .AsNoTracking()
                    .Single(p => p.Id == row.Id).Bets.Where(p => p.Match.StartDate < date).ToArray();


                row.Position = userBets.Count(p => p.BetResult?.Type == "SCORE_MATCH") * 10000 +
                               userBets.Count(p => p.BetResult?.Type == "RESULT_MATCH") * 100 +
                               userBets.Count(p => p.BetResult?.Type == "NO_MATCH") * 1;

                result.Add(row);
            }

            result = result.OrderByDescending(p => p.Position).ToList();

            var position = 1;
            var lastPosition = -1;
            var sameCounter = 1;

            for (var i = 0; i < result.Count; i++)
            {
                if (lastPosition == result[i].Position)
                {
                    result[i].Position = position;
                    sameCounter++;
                }
                else
                {
                    lastPosition = result[i].Position;
                    result[i].Position = i + 1;
                    position = i + sameCounter;
                    sameCounter = 1;
                }
            }

            return result;
        }

        public IEnumerable<DateTime> GetPlayedMatchesDays() => _matchRepository.GetAll(true).Where(p => p.StartDate < DateTime.Now).Select(p => p.StartDate.Date.AddDays(1).AddSeconds(-1)).Distinct().OrderBy(p => p).ToList();
        
        #endregion
        
    }
}
