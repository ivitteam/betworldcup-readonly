﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BetWorldCup.Models;
using BetWorldCup.Services.Repository;
using Microsoft.Extensions.Caching.Memory;

namespace BetWorldCup.Services
{
    public class ChatService : IChatService
    {
        private readonly IMemoryCache _cache;
        private readonly IRepository<ChatDbRow> _chatRepository;

        public ChatService(IMemoryCache cache, IRepository<ChatDbRow> chatRepository)
        {
            _cache = cache;
            _chatRepository = chatRepository;
        }

        public void AddMessage(ChatRow message)
        {
            var list = _cache.Get<Queue<ChatRow>>("ChatCache") ?? new Queue<ChatRow>();
            if (list.Any(p =>
                p.ChatMessage == message.ChatMessage 
                && p.ChatUser == message.ChatUser))
            {
                var timeSpanMessage = TimeSpan.ParseExact(message.ChatTime, "hh\\:mm\\:ss", CultureInfo.InvariantCulture);
                if (list.Where(p =>
                    p.ChatMessage == message.ChatMessage 
                    && p.ChatUser == message.ChatUser)
                    .Any(p => DateClose(timeSpanMessage, p.ChatTime))) 
                    return;
            }
            
            while (list.Count >= 20)
            {
                list.Dequeue();
            }
            list.Enqueue(message);

            _chatRepository.Insert(new ChatDbRow
            {
                ChatTime = DateTime.Now,
                ChatUser = message.ChatUser,
                ChatMessage = message.ChatMessage
            });

            _cache.Set("ChatCache", list);
        }

        public Queue<ChatRow> GetChatMessages()
        {
            var list = _cache.Get<Queue<ChatRow>>("ChatCache");
            if (list != null && list.Count != 0) return list;

            var col = _chatRepository.GetAll(true).OrderByDescending(p => p.ChatTime).Take(20);
            var queue = new Queue<ChatRow>();

            foreach (var message in col.OrderBy(p => p.ChatTime))
            {
                queue.Enqueue(new ChatRow
                {
                    ChatTime = $"{message.ChatTime.Hour:D2}:{message.ChatTime.Minute:D2}:{message.ChatTime.Second:D2}",
                    ChatUser = message.ChatUser,
                    ChatMessage = message.ChatMessage
                });
            }

            list = queue;
            _cache.Set("ChatCache", queue);

            return list;
        }

        private static bool DateClose(TimeSpan time, string strTime)
        {
            var newTime = TimeSpan.ParseExact(strTime, "hh\\:mm\\:ss", CultureInfo.InvariantCulture);
            var timeMax = time.Add(new TimeSpan(0, 0, 1, 0));
            var timeMin = time.Add(new TimeSpan(0, 0, -1, 0));
            return timeMax > newTime && newTime > timeMin;
        }
    }
}
