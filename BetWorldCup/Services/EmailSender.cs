﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace BetWorldCup.Services
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class EmailSender : IEmailSender
    {
        private const string Email = "sender@betworldcup.pl";
        private const string Password = "para$OLKA1";

        public Task SendEmailAsync(string email, string subject, string message)
        {
            const string sentFrom = "BetWorldCup.pl";

            var from = new MailAddress(Email, sentFrom);
            var to = new MailAddress(email);

            // Configure the client:
            var client =
                new SmtpClient("smtp.webio.pl")
                {
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false
                };

            // Create the credentials:
            var credentials =
                new System.Net.NetworkCredential(Email, Password);


            client.EnableSsl = true;
            client.Credentials = credentials;

            // Create the message:
            var mail =
                new MailMessage(from, to)
                {
                    Subject = subject,
                    Body = message,
                    IsBodyHtml = true
                };


            // Send:
            return client.SendMailAsync(mail);
        }
    }
}
