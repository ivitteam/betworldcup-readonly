﻿using System.Linq;
using BetWorldCup.Data;
using BetWorldCup.Models.StatsViewModels;


namespace BetWorldCup.Services.Repository
{
    public class StandingsRepository : Repository<UserStandings>, IStandingsRepository
    {
        public StandingsRepository(ApplicationDbContext Context) : base(Context)
        {
            
        }

        public new void InsertOrUpdate(UserStandings model)
        {
            if (dbSet.Any(p => p.Id == model.Id))
            {
                Update(model);
            }
            else
            {
                Insert(model);
            }
        }
    }
}
