﻿using System;
using System.Collections.Generic;
using System.Linq;
using BetWorldCup.Data;
using Microsoft.EntityFrameworkCore;

namespace BetWorldCup.Services.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly DbSet<T> dbSet;
        protected readonly ApplicationDbContext context;

        // ReSharper disable once MemberCanBeProtected.Global
        public Repository(ApplicationDbContext Context)
        {
            context = Context;
            dbSet = context.Set<T>();
        }
        public IEnumerable<T> GetAll(bool noTracking)
        {
            return noTracking ? dbSet.AsNoTracking() : dbSet;
        }
        public T GetById(object id)
        {
            return dbSet.Find(id);
        }
        public T Insert(T obj)
        {
            dbSet.Add(obj);
            Save();
            return obj;
        }
        public void Delete(object id)
        {
            var entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public void MultipleDelete(IEnumerable<object> ids)
        {
            foreach (var id in ids)
            {
                var entityToDelete = dbSet.Find(id);
                if (context.Entry(entityToDelete).State == EntityState.Detached)
                {
                    dbSet.Attach(entityToDelete);
                }
                dbSet.Remove(entityToDelete);
            }
            Save();
        }

        private void Delete(T entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
            Save();
        }

        public T Update(T obj)
        {
            dbSet.Attach(obj);
            context.Entry(obj).State = EntityState.Modified;

            Save();
            return obj;
        }

        public T InsertOrUpdate(T obj)
        {
            return GetKey(obj) == 0 ? Insert(obj) : Update(obj);
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception dbEx)
            {
                // ReSharper disable once PossibleIntendedRethrow
                   throw dbEx;
            }
        }
        public void Dispose(bool disposing)
        {
            if (!disposing) return;
            context?.Dispose();
        }

        private int GetKey(T entity)
        {
            var keyName = context.Model.FindEntityType(typeof(T)).FindPrimaryKey().Properties
                .Select(x => x.Name).Single();

            return (int)entity.GetType().GetProperty(keyName).GetValue(entity, null);
        }
    }
}
