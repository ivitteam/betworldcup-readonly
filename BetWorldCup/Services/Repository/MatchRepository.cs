﻿using System;
using System.Collections.Generic;
using System.Linq;
using BetWorldCup.Data;
using BetWorldCup.Models.BetViewModels;
using Microsoft.EntityFrameworkCore;

namespace BetWorldCup.Services.Repository
{
    public class MatchRepository : Repository<Match>, IMatchRepository
    {
        public MatchRepository(ApplicationDbContext Context) : base(Context)
        {
        }

        public IEnumerable<Match> GetMatchesForStartScreen(bool noTracking)
        {
            return noTracking ? 
                dbSet
                    .Where(p => p.StartDate > DateTime.Now.AddDays(-2))
                    .OrderBy(p => p.StartDate)
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .Include(p => p.Bets).ThenInclude(p => p.User)
                    .Include(p => p.Bets).ThenInclude(p => p.BetResult)
                    .Include(p => p.Bets).ThenInclude(p => p.Result)
                    .AsNoTracking()
                    .Take(15)
                : 
                dbSet
                    .Where(p => p.StartDate > DateTime.Now.AddDays(-2))
                    .OrderBy(p => p.StartDate)
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .Include(p => p.Bets).ThenInclude(p => p.User)
                    .Include(p => p.Bets).ThenInclude(p => p.BetResult)
                    .Include(p => p.Bets).ThenInclude(p => p.Result)
                    .Take(15);
        }

        public IEnumerable<Match> GetMatchesForAdminStartScreen(bool noTracking)
        {
            return noTracking
                ? dbSet
                    .Where(p => p.StartDate < DateTime.Now.AddHours(2))
                    .OrderByDescending(p => p.StartDate)
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .AsNoTracking()
                : dbSet
                    .Where(p => p.StartDate < DateTime.Now.AddHours(2))
                    .OrderByDescending(p => p.StartDate)
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam);
        }

        public IEnumerable<Match> GetMatchesForAdminModify(bool noTracking)
        {
            return noTracking
                ? dbSet
                    .OrderBy(p => p.StartDate)
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .AsNoTracking()
                : dbSet
                    .OrderBy(p => p.StartDate)
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam);
        }

        public IEnumerable<Match> GetAllMatches(bool noTracking)
        {
            return noTracking
                ? dbSet
                    .OrderBy(p => p.StartDate)
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .Include(p => p.Bets).ThenInclude(p => p.User)
                    .Include(p => p.Bets).ThenInclude(p => p.BetResult)
                    .Include(p => p.Bets).ThenInclude(p => p.Result)
                    .Include(p => p.Result)
                    .AsNoTracking()
                : dbSet
                    .OrderBy(p => p.StartDate)
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .Include(p => p.Bets).ThenInclude(p => p.User)
                    .Include(p => p.Bets).ThenInclude(p => p.BetResult)
                    .Include(p => p.Bets).ThenInclude(p => p.Result)
                    .Include(p => p.Result);
        }
        
        public Match GetSingleMatch(bool noTracking, int id)
        {
            return noTracking
                ? dbSet
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .Include(p => p.Bets).ThenInclude(p => p.User)
                    .Include(p => p.Bets).ThenInclude(p => p.BetResult)
                    .Include(p => p.Bets).ThenInclude(p => p.Result)
                    .AsNoTracking()
                    .Single(p => p.MatchId == id)
                : dbSet
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .Include(p => p.Bets).ThenInclude(p => p.User)
                    .Include(p => p.Bets).ThenInclude(p => p.BetResult)
                    .Include(p => p.Bets).ThenInclude(p => p.Result)
                    .Single(p => p.MatchId == id);
        }

        public Match GetSingleMatchWithTeams(bool noTracking, int id)
        {
            return noTracking
                ? dbSet
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .AsNoTracking()
                    .Single(p => p.MatchId == id)
                : dbSet
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .Single(p => p.MatchId == id);
        }

        public IEnumerable<Match> GetUnfinishedMatches(bool noTracking)
        {
            return noTracking
                ? dbSet
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .Include(p => p.Result)
                    .Where(p => p.StartDate < DateTime.Now && !p.IsFinished)
                    .AsNoTracking()
                : dbSet
                    .Include(p => p.HomeTeam)
                    .Include(p => p.AwayTeam)
                    .Include(p => p.Result)
                    .Where(p => p.StartDate < DateTime.Now && !p.IsFinished);
        }
    }
}
