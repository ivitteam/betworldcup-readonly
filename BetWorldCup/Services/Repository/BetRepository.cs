﻿using System;
using System.Collections.Generic;
using System.Linq;
using BetWorldCup.Data;
using BetWorldCup.Models.BetViewModels;
using Microsoft.EntityFrameworkCore;

namespace BetWorldCup.Services.Repository
{
    public class BetRepository : Repository<Bet>, IBetRepository
    {
        public BetRepository(ApplicationDbContext Context) : base(Context)
        {
            
        }

        public IEnumerable<Bet> GetUserBets(bool noTracking, string userId)
        {
            return noTracking
                ? dbSet
                    .Include(p => p.BetResult)
                    .Include(p => p.Match).ThenInclude(p => p.HomeTeam)
                    .Include(p => p.Match).ThenInclude(p => p.AwayTeam)
                    .AsNoTracking()
                    .Where(p => p.User.Id == userId)
                : dbSet
                    .Include(p => p.BetResult)
                    .Include(p => p.Match).ThenInclude(p => p.HomeTeam)
                    .Include(p => p.Match).ThenInclude(p => p.AwayTeam)
                    .Where(p => p.User.Id == userId);
        }

        public IEnumerable<Bet> GetBetsOfStartedMatches(bool noTracking)
        {
            return noTracking
                ? dbSet
                    .AsNoTracking()
                    .Include(p => p.Match)
                    .Where(p => p.Match.StartDate < DateTime.Now)
                : dbSet
                    .Include(p => p.Match)
                    .Where(p => p.Match.StartDate < DateTime.Now);
        }

        public IEnumerable<Bet> GetMatchBets(bool noTracking, int matchId)
        {
            return noTracking
                ? dbSet
                    .AsNoTracking()
                    .Include(p => p.Result)
                    .Where(p => p.Match.MatchId == matchId)
                : dbSet
                    .Include(p => p.Result)
                    .Where(p => p.Match.MatchId == matchId);
        }

        public void UpdateBetResults(Match match)
        {
            var bets = context.Bets.Where(p => p.Match.MatchId == match.MatchId);
            foreach (var bet in bets)
            {
                if (match.Result != bet.Result)
                    bet.BetResult = context.BetResults.Single(p => p.Type == "NO_MATCH");
                else if (match.HomeScore != bet.HomeScore || match.AwayScore != bet.AwayScore)
                    bet.BetResult = context.BetResults.Single(p => p.Type == "RESULT_MATCH");
                else
                    bet.BetResult = context.BetResults.Single(p => p.Type == "SCORE_MATCH");
                Save();
            }
        }
    }
}
