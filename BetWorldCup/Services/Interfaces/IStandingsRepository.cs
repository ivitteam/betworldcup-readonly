﻿using System.Collections.Generic;
using BetWorldCup.Models.StatsViewModels;

namespace BetWorldCup.Services.Repository
{
    public interface IStandingsRepository
    {
        void InsertOrUpdate(UserStandings model);
        void MultipleDelete(IEnumerable<object> ids);
    }
}