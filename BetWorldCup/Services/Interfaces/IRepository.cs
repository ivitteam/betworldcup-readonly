﻿using System.Collections.Generic;

namespace BetWorldCup.Services.Repository
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll(bool noTracking);
        T GetById(object id);
        T Insert(T obj);
        void Delete(object id);
        T Update(T obj);
        void Dispose(bool disposing);
        T InsertOrUpdate(T obj);
        void MultipleDelete(IEnumerable<object> ids);
    }
}