﻿using System.Collections.Generic;
using BetWorldCup.Models;
using BetWorldCup.Models.BetViewModels;

namespace BetWorldCup.Services
{
    public interface IAdminService
    {
        IEnumerable<Match> GetMatchesForStartScreen();
        void UpdateMatchScore(Match model);
        IEnumerable<Team> GetNotEliminatedTeams();
        Match AddMatch(Match model);
        IEnumerable<Match> GetMatchesForAdminModify();
        Team GetTeamById(int id);
        void DeleteMatch(int id);
        Match GetMatch(int id);
        IEnumerable<Team> GetTeams();
        void UpdateEliminatedStatus(Team model);
        void UpdateFunUser(ApplicationUser model);
        IEnumerable<ApplicationUser> GetUsers();
        void DeleteUser(string id);
    }
}