﻿using System.Threading.Tasks;

namespace BetWorldCup.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}