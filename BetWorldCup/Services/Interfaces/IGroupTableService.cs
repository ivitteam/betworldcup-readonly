﻿using System.Threading.Tasks;
using BetWorldCup.Models.RESTApi.Table;

namespace BetWorldCup.Services.RESTApi
{
    public interface IGroupTableService
    {
        Task<RootObject> GetTable (int leagueId);
    }
}