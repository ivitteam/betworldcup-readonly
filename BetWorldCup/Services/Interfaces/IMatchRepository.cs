﻿using System.Collections.Generic;
using BetWorldCup.Models.BetViewModels;

namespace BetWorldCup.Services.Repository
{
    public interface IMatchRepository
    {
        IEnumerable<Match> GetMatchesForStartScreen(bool noTracking);
        IEnumerable<Match> GetAll(bool noTracking);
        Match InsertOrUpdate(Match obj);
        Match GetById(object id);
        Match Insert(Match obj);
        void Delete(object id);
        Match Update(Match obj);
        void Dispose(bool disposing);
        IEnumerable<Match> GetAllMatches(bool noTracking);
        Match GetSingleMatch(bool noTracking, int id);
        Match GetSingleMatchWithTeams(bool noTracking, int id);
        IEnumerable<Match> GetMatchesForAdminStartScreen(bool noTracking);
        IEnumerable<Match> GetMatchesForAdminModify(bool noTracking);
        IEnumerable<Match> GetUnfinishedMatches(bool noTracking);
    }
}