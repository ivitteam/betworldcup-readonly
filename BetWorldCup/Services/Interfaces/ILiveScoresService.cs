﻿using System.Threading;
using System.Threading.Tasks;
using BetWorldCup.Models.RESTApi;

namespace BetWorldCup.Services.RESTApi
{
    public interface ILiveScoresService
    {
        Task<Response> GetAll(CancellationToken cancellationToken);
        Task<Response> GetForLeague(CancellationToken cancellationToken, int leagueId);
    }
}