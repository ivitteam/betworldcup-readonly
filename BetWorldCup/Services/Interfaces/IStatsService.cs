﻿using System;
using System.Collections.Generic;
using BetWorldCup.Models;
using BetWorldCup.Models.BetViewModels;
using BetWorldCup.Models.RESTApi.Table;
using BetWorldCup.Models.StatsViewModels;

namespace BetWorldCup.Services
{
    public interface IStatsService
    {
        IEnumerable<UserMatchResult> GetUserMatchResults(int matchId, string currentUserName);
        List<object> PieChartMatchResultRatio(int matchId);
        List<object> BarChartMatchScoreRatio(int matchId);
        List<object> PieChartUserPointsRatio(int matchId);
        List<object> PieChartOverUnderRatio(int matchId);
        int GetUserScoreHits(ApplicationUser user, DateTime date);
        int GetUserResultHits(ApplicationUser user, DateTime date);
        int GetUserMisses(ApplicationUser user, DateTime date);
        int GetUserNoBets(ApplicationUser user, DateTime date);
        IEnumerable<ClassificationRow> GetActuallFullClassification(string currentUserName);
        void UpdateUserPointsAndPositions(DateTime date);
        ApplicationUser GetUserByName(string userName);
        List<object> LineGetPointsHistoryForUser(string userName);
        List<object> LineGetPositionHistoryForUser(string userName);
        IEnumerable<DateTime> GetPlayedMatchesDays();
        void UpdateUserPointsAndPositionsForSeed(DateTime date);
        List<object> BarChartUserBetsPoints(string userName);
        List<object> PieChartOverUnderRatioAllScores();
        List<object> LineGetPointsHistoryForAllUser(string userName);
        List<object> PieChartUserPointsRatioAllMatch();
        List<object> LineGetPositionHistoryForAllUser(string userName);
        IEnumerable<ClassificationRow> GetAllUserScoreHits();
        IEnumerable<ClassificationRow> GetAllUserResulHits();
        IEnumerable<ClassificationRow> GetAllUserMisses();
        IEnumerable<ClassificationRow> GetBestMatches();
        IEnumerable<ClassificationRow> GetWorstMatches();
        IEnumerable<ClassificationRow> GetBestTeams();
        IEnumerable<ClassificationRow> GetWorstTeams();
        Team GetTeam(int teamId);
        IEnumerable<Match> GetTeamMatches(int teamId);
        IEnumerable<Table> GetTable(int leagueId);
        List<object> PieChartTeamAvarageGoals(int teamId);
        List<object> PieChartTeamResults(int teamId);
    }
}