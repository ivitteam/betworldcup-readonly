﻿using System.Collections.Generic;
using BetWorldCup.Models.BetViewModels;

namespace BetWorldCup.Services.Repository
{
    public interface IBetRepository
    {
        IEnumerable<Bet> GetAll(bool noTracking);
        Bet GetById(object id);
        Bet Insert(Bet obj);
        void Delete(object id);
        Bet Update(Bet obj);
        Bet InsertOrUpdate(Bet obj);
        void Dispose(bool disposing);
        IEnumerable<Bet> GetUserBets(bool noTracking, string userId);
        IEnumerable<Bet> GetMatchBets(bool noTracking, int matchId);
        void UpdateBetResults(Match match);
        void MultipleDelete(IEnumerable<object> ids);
        IEnumerable<Bet> GetBetsOfStartedMatches(bool noTracking);
    }
}