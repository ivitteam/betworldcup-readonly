﻿using System.Collections.Generic;

namespace BetWorldCup.Services
{
    public interface IGoogleAnalyticsApiService
    {
        List<object> GetDailyVisits();
        List<object> GetHourAvarageVisits();
        List<object> GetBrowsers();
        List<object> GetDevices();
        List<object> GetMostVisitedPages();
    }
}