﻿using System.Collections.Generic;
using BetWorldCup.Models;

namespace BetWorldCup.Services
{
    public interface IChatService
    {
        void AddMessage(ChatRow message);
        Queue<ChatRow> GetChatMessages();
    }
}