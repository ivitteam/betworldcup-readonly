﻿using System.Collections.Generic;
using BetWorldCup.Models.BetViewModels;

namespace BetWorldCup.Services
{
    public interface IHomeService
    {
        IEnumerable<Match> GetMatchesForStartScreen();
        Bet PlaceBet(Bet model, string email, int? matchId);
        IEnumerable<Match> GetAllMatches();
        Match GetSingleMatch(int id);
        string[] GetUserNames();
        string[] GetMatchIds();
    }
}