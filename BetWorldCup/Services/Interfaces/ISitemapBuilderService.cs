﻿using System;
using BetWorldCup.Models;

namespace BetWorldCup.Services
{
    public interface ISitemapBuilderService
    {
        void AddUrl(string url, DateTime? modified = null, ChangeFrequency? changeFrequency = null, double? priority = null);
        string ToString();
    }
}