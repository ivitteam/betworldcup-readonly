﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using BetWorldCup.Models;
using BetWorldCup.Models.AccountViewModels;
using BetWorldCup.Services;
using NToastNotify;
using BetWorldCup.Helpers;

namespace BetWorldCup.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly IToastNotification _toastNotification;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            IToastNotification toastNotification)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _toastNotification = toastNotification;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("zaloguj")]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            if (TempData["ChangedPassword"] != null)
            {
                TempData["ChangedPassword"] = null;
                _toastNotification.AddSuccessToastMessage("Hasło zostało zmienione, zaloguj się ponownie używając nowego hasła.", new ToastrOptions { Title = "Zmiana hasła" });
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("zaloguj")]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    return RedirectToLocal(returnUrl);
                }
                if (result.IsNotAllowed)
                {
                    ViewData["Email"] = model.Email;
                    return RedirectToAction(nameof(SendConfirmationEmail), new {email = model.Email});
                }

                ModelState.AddModelError(string.Empty, "Nieudana próba logowania");
                return View(model);

            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("zdalne-logowanie")]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Action(nameof(ExternalLoginCallback), "Account", new { returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("zdalne-logowanie-callback")]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                return RedirectToAction(nameof(Login));
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
            if (result.Succeeded)
            {
                return RedirectToLocal(returnUrl);
            }

            // If the user does not have an account, then ask the user to create an account.
            ViewData["ReturnUrl"] = returnUrl;
            ViewData["LoginProvider"] = info.LoginProvider;
            var email = info.Principal.FindFirstValue(ClaimTypes.Email);
            var name = info.Principal.FindFirstValue(ClaimTypes.Name);
            name = string.IsNullOrEmpty(name) ? string.IsNullOrEmpty(email) ? "" : email.Split('@')[0] : name.RemoveDiacritics();
            return View("ExternalLogin", new ExternalLoginViewModel { Email = email, Name = name});
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("zdalne-logowanie-potwierdzenie")]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginViewModel model, string returnUrl = null)
        {
            var ifNameExist = _userManager.Users.FirstOrDefault(a => a.Name == model.Name);
            if (ifNameExist != null)
            {
                ModelState.AddModelError(string.Empty, "Login jest już zajęty.");
            }
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    throw new ApplicationException("Error loading external login information during confirmation.");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, Name = model.Name, FunAccount = true, EmailConfirmed = true, Position = 999, Points = 0};
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(nameof(ExternalLogin), model);
        }

       
        [HttpGet]
        [AllowAnonymous]
        [Route("zarejestruj")]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("zarejestruj")]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            var ifNameExist = _userManager.Users.FirstOrDefault(a=>a.Name==model.Name);
            if (ifNameExist != null)
            {
                 ModelState.AddModelError(string.Empty, "Login jest już zajęty.");
            }
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, Name = model.Name, FunAccount = true, Position = 999, Points = 0 };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {                    

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);                    
                    var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                    await _emailSender.SendEmailConfirmationAsync(model.Email, callbackUrl);
                    return RedirectToLocal(returnUrl);
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("weryfikacja-email")]
        public IActionResult SendConfirmationEmail(string email)
        {
            ViewData["Email"] = email;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("weryfikacja-email")]
        public async Task<IActionResult> SendEmailConfirm(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
            await _emailSender.SendEmailConfirmationAsync(user.Email, callbackUrl);

            return RedirectToAction(nameof(Login));
        }
        
        [Route("wyloguj")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(Login));
        }
        
        [HttpGet]
        [AllowAnonymous]
        [Route("email-potwierdzajacy")]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(Login));
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Nie można znaleźć użytkownika o emailu: '{userId}'.");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("przypomnienie-hasla")]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("przypomnienie-hasla")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    _toastNotification.AddErrorToastMessage($"Nie znaleziono użytkownika z kontem: {model.Email}");
                    return RedirectToAction(nameof(ForgotPasswordConfirmation));
                }

                // For more information on how to enable account confirmation and password reset please
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
                await _emailSender.SendEmailAsync(model.Email, "Przypomnienie hasła",
                   $"Proszę klinknąć link aby przypomnieć hasło: <a href='{callbackUrl}'>LINK</a>");
                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("przypomnienie-hasla-potwierdzenie")]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("reset-hasla")]
        public IActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Code = code };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("reset-hasla")]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            AddErrors(result);
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("reset-hasla-potwierdzenie")]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }


        [AllowAnonymous]
        [Route("error")]
        public IActionResult Error(int? statusCode = null)
        {
            switch (statusCode)
            {
                case 404:
                    ViewBag.MessageTitle = "Nie znaleziono strony!";
                    ViewBag.Message = "Serwer nie odnalazł zasobu według podanego URL ani niczego co by wskazywało na istnienie takiego zasobu w przeszłości.";
                    break;
                case 500:
                    ViewBag.MessageTitle = "Wewnętrzny błąd serwera!";
                    ViewBag.Message = "Serwer napotkał niespodziewane trudności, które uniemożliwiły zrealizowanie żądania.";
                    break;
                default:
                    ViewBag.MessageTitle = "Wyjątek krytyczny!";
                    ViewBag.Message = "Pojawił się wyjątek krytyczny, pracujemy nad jego usunięciem.";
                    break;
            }

            return View();
        }

        [HttpGet]
        [Route("zmien-haslo")]
        public IActionResult ChangePassword()
        {
            var model = new ChangePasswordViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("zmien-haslo")]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var user = await _userManager.FindByEmailAsync(model.Email);
            var CheckCorrectOldPassword = await _signInManager.PasswordSignInAsync(model.Email, model.OldPassword,true, lockoutOnFailure: false);
                
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "Błędnie podany E-Mail.");
            }
            if (!CheckCorrectOldPassword.Succeeded)
            {
                ModelState.AddModelError(string.Empty, "Błędnie podane stare hasło.");
            }

            if (!ModelState.IsValid) return View(model);

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, token, model.Password);

            if (!result.Succeeded) return View(model);

            await _signInManager.SignOutAsync();
            TempData["ChangedPassword"] = "TRUE";
            return RedirectToAction(nameof(Login));
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        #endregion
    }
}
