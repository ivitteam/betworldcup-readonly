﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BetWorldCup.Controllers
{
    [AllowAnonymous]
    public class DocsController : Controller
    {
        [Route("regulamin")]
        public IActionResult Rules()
        {
            return View();
        }

        [Route("polityka-prywatnosci")]
        public IActionResult PrivacyPolicy()
        {
            return View();
        }
    }
}