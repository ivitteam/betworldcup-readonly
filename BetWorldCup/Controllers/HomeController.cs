﻿using System;
using System.Collections.Generic;
using System.Linq;
using BetWorldCup.Models.BetViewModels;
using BetWorldCup.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NToastNotify;

namespace BetWorldCup.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IHomeService _homeService;
        private readonly IToastNotification _toastNotification;
        private readonly ILogger<HomeController> _logger;

        public HomeController(IHomeService homeService, IToastNotification toastNotification, ILogger<HomeController> logger)
        {
            _homeService = homeService;
            _toastNotification = toastNotification;
            _logger = logger;
        }

        #region Widoki
        
        public IActionResult Index()
        {
            IEnumerable<Match> model = new List<Match>();

            try
            {
                model = _homeService.GetMatchesForStartScreen();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać meczy, spróbuj ponownie!",
                    new ToastrOptions {Title = "Błąd!"});
                _logger.LogError(e, "Nie udało się pobrać meczy. Strona startowa.");
            }

            return View(model);
        }

        [Route("wszystkie-mecze")]
        public IActionResult AllMatches()
        {
            IEnumerable<Match> model = new List<Match>();

            try
            {
                model = _homeService.GetAllMatches().ToArray();
                var closest = model.OrderBy(p => Math.Abs((p.StartDate - DateTime.Now).Ticks)).First();
                ViewBag.DivId = $"{closest.HomeTeam.ShortName}-{closest.AwayTeam.ShortName}";
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać meczy, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać meczy. Wszystkie mecze.");
            }

            return View(model.OrderBy(p => p.StartDate));
        }

        [Route("mecz")]
        public IActionResult SingleMatch(int id)
        {
            var model = new Match();

            try
            {
                model = _homeService.GetSingleMatch(id);
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać meczu, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać meczu. Strona startowa.");
            }

            return View(model);
        }

        #endregion

        #region Akcje

        [Route("obstaw")]
        public JsonResult PlaceBet(Bet model, int matchId)
        {
            try
            {
                var bet = _homeService.PlaceBet(model, User.Identity.Name, matchId);
                return Json(new { ok = true, betId = bet.BetId });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się obstawić.");
                return Json(new { ok = false, message = "Nie udało się obstawić. Spróbuj ponownie." });
            }
        }

        #endregion
    }
}