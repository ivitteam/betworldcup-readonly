﻿using BetWorldCup.Models;
using BetWorldCup.Services;
using Microsoft.AspNetCore.Mvc;

namespace BetWorldCup.Controllers
{
    public class ChatController : Controller
    {
        private readonly IChatService _chatService;

        public ChatController(IChatService chatService)
        {
            _chatService = chatService;
        }
        
        public EmptyResult InsertChatMessage(ChatRow model)
        {
            _chatService.AddMessage(model);
            return new EmptyResult();
        }

        public JsonResult GetChatMessages()
        {
            return Json(_chatService.GetChatMessages());
        }
    }
}