﻿using System;
using System.Collections.Generic;
using System.Linq;
using BetWorldCup.Models;
using BetWorldCup.Models.StatsViewModels;
using BetWorldCup.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NToastNotify;

namespace BetWorldCup.Controllers
{
    [Authorize]
    public class StatsController : Controller
    {
        private readonly IStatsService _statsService;
        private readonly IToastNotification _toastNotification;
        private readonly ILogger<StatsController> _logger;
        private readonly IGoogleAnalyticsApiService _googleAnalyticsApiService;

        public StatsController(IStatsService statsService,
            IToastNotification toastNotification,
            ILogger<StatsController> logger,
            IGoogleAnalyticsApiService googleAnalyticsApiService)
        {
            _statsService = statsService;
            _toastNotification = toastNotification;
            _logger = logger;
            _googleAnalyticsApiService = googleAnalyticsApiService;
        }

        #region Widoki
        
        [Route("statystyki-uzytkownika")]
        public IActionResult UserData(string user)
        {
            var model = new ApplicationUser();

            try
            {
                model = _statsService.GetUserByName(user);

                ViewBag.Scores = _statsService.GetUserScoreHits(model, DateTime.Now);
                ViewBag.Results = _statsService.GetUserResultHits(model, DateTime.Now);
                ViewBag.Missess = _statsService.GetUserMisses(model, DateTime.Now);
                ViewBag.NoBets = _statsService.GetUserNoBets(model, DateTime.Now);
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać użytkownika, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać użytkownika. UserData - statystyki");
            }

            return View(model);
        }
        
        [Route("statystyki")]
        public IActionResult ResultAllStats()
        {
            return View();
        }

        [Route("druzyna")]
        public IActionResult TeamData(int teamId)
        {
            try
            {
                var model = _statsService.GetTeam(teamId);
                var matches = _statsService.GetTeamMatches(teamId).OrderBy(p => p.StartDate).ToList();
                ViewBag.Matches = matches;
                ViewBag.Table = _statsService.GetTable(matches.OrderBy(p => p.StartDate).First().LeagueId);
                return View(model);
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać danych drużyny, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać danych drużyny. TeamData - statystyki");
            }
            return View();
        }
        #endregion

        #region Tabele

        public IActionResult MatchResultTable(int matchId)
        {
            IEnumerable<UserMatchResult> model = new List<UserMatchResult>();

            try
            {
                model = _statsService.GetUserMatchResults(matchId, User.Identity.Name);
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać tabeli wyników meczy, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać tabeli wyników meczy. MatchResultTable - statystyki");
            }

            return PartialView("Tables/_SingleMatchBets", model);
        }
        
        public IActionResult FullClassificationTable(bool overflow = false)
        {
            IEnumerable<ClassificationRow> model = new List<ClassificationRow>();

            try
            {
                model = _statsService.GetActuallFullClassification(User.Identity.Name);
                ViewBag.Overflow = overflow ? " table-sidebar" : "";
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać klasyfikacji, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać klasyfikacji. FullClassificationTable - statystyki");
            }
            
            return PartialView("Tables/_FullClassification", model);
        }
        
        public IActionResult TopScoreClassificationTable()
        {
            IEnumerable<ClassificationRow> model = new List<ClassificationRow>();

            try
            {
                model = _statsService.GetAllUserScoreHits();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać klasyfikacji najwięcej trafień, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać klasyfikacji. TopScoreClassificationTable - statystyki");
            }
            return PartialView("Tables/_TopFiveClassifictaion", model);
        }

        public IActionResult TopResultClassificationTable()
        {
            IEnumerable<ClassificationRow> model = new List<ClassificationRow>();
            try
            {
                model = _statsService.GetAllUserResulHits();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać klasyfikacji najwięcej rezultatów, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać klasyfikacji. TopResultClassificationTable - statystyki");
            }
            return PartialView("Tables/_TopFiveClassifictaion", model);
        }

        public IActionResult TopMissesClassificationTable()
        {
            IEnumerable<ClassificationRow> model = new List<ClassificationRow>();
            try
            {
                model = _statsService.GetAllUserMisses();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać klasyfikacji błędnych rezultatów, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać klasyfikacji. TopMissesClassificationTable - statystyki");
            }
            return PartialView("Tables/_TopFiveClassifictaion", model);
        }
        
        public IActionResult TopMatchesTable()
        {
            IEnumerable<ClassificationRow> model = new List<ClassificationRow>();
            try
            {
                model = _statsService.GetBestMatches();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać klasyfikacji najlepszych meczów, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać klasyfikacji. TopMatches - statystyki");
            }
            return PartialView("Tables/_TopMatches", model);
        }

        public IActionResult WorstMatchesTable()
        {
            IEnumerable<ClassificationRow> model = new List<ClassificationRow>();
            try
            {
                model = _statsService.GetWorstMatches();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać klasyfikacji najgorszych meczów, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać klasyfikacji. WorstMatches - statystyki");
            }
            return PartialView("Tables/_TopMatches", model);
        }

        public IActionResult TopTeamsTable()
        {
            IEnumerable<ClassificationRow> model = new List<ClassificationRow>();
            try
            {
                model = _statsService.GetBestTeams();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać klasyfikacji najlepszych drużyn, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać klasyfikacji. TopTeamsTable - statystyki");
            }
            return PartialView("Tables/_TopTeams", model);
        }

        public IActionResult WorstTeamsTable()
        {
            IEnumerable<ClassificationRow> model = new List<ClassificationRow>();
            try
            {
                model = _statsService.GetWorstTeams();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać klasyfikacji najgorszych drużyn, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać klasyfikacji. WorstTeamsTable - statystyki");
            }
            return PartialView("Tables/_TopTeams", model);
        }
        
        #endregion

        #region Wykresy

        [HttpPost]
        public JsonResult PieChartMatchResultRatio(int matchId)
        {
            try
            {
                var chartData = _statsService.PieChartMatchResultRatio(matchId);
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: PieChartMatchResultRatio.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu kołowego Typowane rezultaty. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult BarChartMatchScoreRatio(int matchId)
        {
            try
            {
                var chartData = _statsService.BarChartMatchScoreRatio(matchId);
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: BarChartMatchScoreRatio.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do słupkowego kołowego Typowane wyniki. Spróbuj ponownie." });
            }

        }

        [HttpPost]
        public JsonResult BarChartUserBetsPoints(string userName)
        { 
            try
            {
                var chartData = _statsService.BarChartUserBetsPoints(userName);
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: BarChartUserBetsPoints.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu kołowego Punkty za poszczególne mecze. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult PieChartUserPointsRatio(int matchId)
        {
            try
            {
                var chartData = _statsService.PieChartUserPointsRatio(matchId);
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: PieChartUserPointsRatio.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu kołowego Wyniki typowania. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult PieChartOverUnderRatio(int matchId)
        {
            try
            {
                var chartData = _statsService.PieChartOverUnderRatio(matchId);
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: PieChartOverUnderRatio.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu kołowego Powyżej/Poniżej 2.5 gola. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult LineChartUserPointsHistory(string userName)
        {
            try
            {
                var chartData = _statsService.LineGetPointsHistoryForUser(userName);
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: LineChartUserPointsHistory.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu liniowego Punkty względem czasu. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult LineChartUserPositionHistory(string userName)
        {
            try
            {
                var chartData = _statsService.LineGetPositionHistoryForUser(userName);
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: LineChartUserPositionHistory.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu liniowego Pozycja względem czasu. Spróbuj ponownie." });
            }
        }
        
        [HttpPost]
        public JsonResult PieChartOverUnderRatioAllScores()
        {
            try
            {
                var chartData = _statsService.PieChartOverUnderRatioAllScores();
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: PieChartOverUnderRatioAllScores.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu kołowego Powyżej/Poniżej 2.5 gola. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult LineChartUserPointsHistoryAllUsers(string userName)
        {
            try
            {
                var chartData = _statsService.LineGetPointsHistoryForAllUser(userName);
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: LineChartUserPointsHistoryAllUsers.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu liniowego Punkty względem czasu. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult PieChartUserPointsRatioAllMatch()
        {
            try
            {
                var chartData = _statsService.PieChartUserPointsRatioAllMatch();
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: PieChartUserPointsRatioAllMatch.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu kołowego Wyniki typowania. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult LineChartUserPositionHistoryForAllUsers(string userName)
        {
            try
            {
                var chartData = _statsService.LineGetPositionHistoryForAllUser(userName);
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: LineGetPositionHistoryForAllUser.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu liniowego Pozycja względem czasu. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult PieChartAvarageTeamGoals(int teamId)
        {
            try
            {
                var chartData = _statsService.PieChartTeamAvarageGoals(teamId);
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: PieChartAvarageTeamGoals.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu kołowego średnia ilość bramek. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult PieChartTeamResults(int teamId)
        {
            try
            {
                var chartData = _statsService.PieChartTeamResults(teamId);
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: PieChartTeamResults.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu kołowego wyniki drużyny. Spróbuj ponownie." });
            }
        }
        #endregion

        #region Google Analytics

        [HttpPost]
        public JsonResult LineChartLastMonthVisits()
        {
            try
            {
                var chartData = _googleAnalyticsApiService.GetDailyVisits();
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: LineChartLastMonthVisits.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu liniowego Dzienny wykres odwiedzin. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult LineChartHourAvarageVisits()
        {
            try
            {
                var chartData = _googleAnalyticsApiService.GetHourAvarageVisits();
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: LineChartHourAvarageVisits.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu liniowego Średnia ilość odwiedzin. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult BarChartMostVisited()
        {
            try
            {
                var chartData = _googleAnalyticsApiService.GetMostVisitedPages();
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: BarChartMostVisited.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu słupkowego Najczęściej odwiedzane strony. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult BrowsersLastMonth()
        {
            try
            {
                var chartData = _googleAnalyticsApiService.GetBrowsers();
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: BrowsersLastMonth.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu kołowego Przeglądarki w zeszłym miesiącu. Spróbuj ponownie." });
            }
        }

        [HttpPost]
        public JsonResult DevicesLastMonth()
        {
            try
            {
                var chartData = _googleAnalyticsApiService.GetDevices();
                return Json(new { ok = true, chData = chartData });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się pobrać danych do wykresu: DevicesLastMonth.");
                return Json(new { ok = false, message = "Nie udało się pobrać danych do wykresu urządzenia w zeszłym miesiącu. Spróbuj ponownie." });
            }
        }

        #endregion
    }
}