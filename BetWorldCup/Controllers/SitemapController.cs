﻿using System;
using BetWorldCup.Models;
using BetWorldCup.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BetWorldCup.Controllers
{
    [AllowAnonymous]
    public class SitemapController : Controller
    {
        private readonly ISitemapBuilderService _sitemapBuilderService;
        private readonly IHomeService _homeService;
        private const string BASE_URL = "https://betworldcup.pl/";

        public SitemapController(ISitemapBuilderService sitemapBuilderService, IHomeService homeService)
        {
            _sitemapBuilderService = sitemapBuilderService;
            _homeService = homeService;
        }

        [Route("sitemap")]
        public IActionResult Sitemap()
        {
            // HOME CONTROLLER

            // hompage
            _sitemapBuilderService.AddUrl(BASE_URL, DateTime.UtcNow, ChangeFrequency.Daily, 1.0);
            
            _sitemapBuilderService.AddUrl(BASE_URL + "wszystkie-mecze", DateTime.UtcNow, ChangeFrequency.Daily, 0.8);
            foreach (var matchId in _homeService.GetMatchIds())
            {
                _sitemapBuilderService.AddUrl(BASE_URL + $"mecz?{matchId}", DateTime.UtcNow, ChangeFrequency.Daily, 0.3);
            }

            // ACCOUNT CONTROLLER

            _sitemapBuilderService.AddUrl(BASE_URL + "zaloguj", DateTime.UtcNow, ChangeFrequency.Daily, 0.5);
            _sitemapBuilderService.AddUrl(BASE_URL + "zdalne-logowanie", DateTime.UtcNow, ChangeFrequency.Daily, 0.1);
            _sitemapBuilderService.AddUrl(BASE_URL + "zdalne-logowanie-potwierdzenie", DateTime.UtcNow, ChangeFrequency.Daily, 0.1);
            _sitemapBuilderService.AddUrl(BASE_URL + "zarejestruj", DateTime.UtcNow, ChangeFrequency.Daily, 0.5);
            _sitemapBuilderService.AddUrl(BASE_URL + "weryfikacja-email", DateTime.UtcNow, ChangeFrequency.Daily, 0.1);
            _sitemapBuilderService.AddUrl(BASE_URL + "email-potwierdzajacy", DateTime.UtcNow, ChangeFrequency.Daily, 0.1);
            _sitemapBuilderService.AddUrl(BASE_URL + "przypomnienie-hasla", DateTime.UtcNow, ChangeFrequency.Daily, 0.1);
            _sitemapBuilderService.AddUrl(BASE_URL + "przypomnienie-hasla-potwierdzenie", DateTime.UtcNow, ChangeFrequency.Daily, 0.1);
            _sitemapBuilderService.AddUrl(BASE_URL + "reset-hasla", DateTime.UtcNow, ChangeFrequency.Daily, 0.1);
            _sitemapBuilderService.AddUrl(BASE_URL + "reset-hasla-potwierdzenie", DateTime.UtcNow, ChangeFrequency.Daily, 0.1);
            _sitemapBuilderService.AddUrl(BASE_URL + "error", DateTime.UtcNow, ChangeFrequency.Daily, 0.1);
            _sitemapBuilderService.AddUrl(BASE_URL + "zmien-haslo", DateTime.UtcNow, ChangeFrequency.Daily, 0.5);

            //STATS CONTROLLER
            
            _sitemapBuilderService.AddUrl(BASE_URL + "statystyki", DateTime.UtcNow, ChangeFrequency.Daily, 0.7);
            foreach (var uname in _homeService.GetUserNames())
            {
                _sitemapBuilderService.AddUrl(BASE_URL + $"statystyki-uzytkownika?{uname}", DateTime.UtcNow, ChangeFrequency.Daily, 0.3);
            }

            //ADMIN CONTROLLER
            
            _sitemapBuilderService.AddUrl(BASE_URL + "panel-administratora/wyniki", DateTime.UtcNow, ChangeFrequency.Daily, 0.3);
            _sitemapBuilderService.AddUrl(BASE_URL + "panel-administratora/mecze", DateTime.UtcNow, ChangeFrequency.Daily, 0.3);
            _sitemapBuilderService.AddUrl(BASE_URL + "panel-administratora/druzyny", DateTime.UtcNow, ChangeFrequency.Daily, 0.3);
            _sitemapBuilderService.AddUrl(BASE_URL + "panel-administratora/uzytkownicy", DateTime.UtcNow, ChangeFrequency.Daily, 0.3);
            _sitemapBuilderService.AddUrl(BASE_URL + "panel-administratora/statystyki-google", DateTime.UtcNow, ChangeFrequency.Daily, 0.3);
            
            //DOCS CONTROLLER
            
            _sitemapBuilderService.AddUrl(BASE_URL + "regulamin", DateTime.UtcNow, ChangeFrequency.Daily, 0.4);
            _sitemapBuilderService.AddUrl(BASE_URL + "polityka-prywatnosci", DateTime.UtcNow, ChangeFrequency.Daily, 0.4);

            // generate the sitemap xml
            var xml = _sitemapBuilderService.ToString();
            return Content(xml, "text/xml");
        }
    }
}