﻿using System;
using System.Collections.Generic;
using System.Linq;
using BetWorldCup.Models;
using BetWorldCup.Models.BetViewModels;
using BetWorldCup.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using NToastNotify;

namespace BetWorldCup.Controllers
{
    [Authorize(Roles = "ADMIN")]
    [Route("panel-administratora")]
    public class AdminController : Controller
    {
        private readonly IAdminService _adminService;
        private readonly IStatsService _statsService;
        private readonly IToastNotification _toastNotification;
        private readonly ILogger<AdminController> _logger;

        public AdminController(IAdminService adminService, 
            IStatsService statsService, 
            IToastNotification toastNotification, 
            ILogger<AdminController> logger)
        {
            _adminService = adminService;
            _statsService = statsService;
            _toastNotification = toastNotification;
            _logger = logger;
        }

        #region Widoki
        
        [Route("wyniki")]
        public IActionResult Index()
        {
            IEnumerable<Match> model = new List<Match>();

            try
            {
                model = _adminService.GetMatchesForStartScreen();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać meczy, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać meczy. Index - panel administratora.");
            }

            return View(model);
        }

        [Route("mecze")]
        public IActionResult ListOfMatches()
        {
            IEnumerable<Match> model = new List<Match>();

            try
            {
                if (TempData["Message"] != null)
                {
                    if (TempData["Message"].ToString() == "WARNING")
                        _toastNotification.AddWarningToastMessage("Nie można usunąć meczu, ponieważ są już do niego przypisane zakłady.",
                            new ToastrOptions { Title = "Ostrzeżenie!" });
                    else
                        _toastNotification.AddErrorToastMessage("Nie udało się usunąć meczu.",
                            new ToastrOptions { Title = "Błąd!" });
                }

                ViewBag.Teams = _adminService.GetNotEliminatedTeams().Select(p => new SelectListItem
                {
                    Text = $"{p.ShortName} - {p.LongName}",
                    Value = p.TeamId.ToString()
                }).OrderBy(p => p.Text);

                model = _adminService.GetMatchesForAdminModify();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać meczy, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać meczy. ListOfMatches - panel administratora.");
            }

            return View(model);
        }

        [Route("druzyny")]
        public IActionResult Teams()
        {
            IEnumerable<Team> model = new List<Team>();

            try
            {
                model = _adminService.GetTeams();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać drużyn, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać drużyn. Teams - panel administratora.");
            }

            return View(model);
        }

        [Route("uzytkownicy")]
        public IActionResult Users()
        {
            IEnumerable<ApplicationUser> model = new List<ApplicationUser>();

            try
            {
                model = _adminService.GetUsers();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać użytkowników, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać użytkowników. Users - panel administratora.");
            }

            return View(model);
        }
        
        [Route("usuwanie-uzytkownikow")]
        public IActionResult DeleteUsers()
        {
            IEnumerable<ApplicationUser> model = new List<ApplicationUser>();

            try
            {
                model = _adminService.GetUsers();
            }
            catch (Exception e)
            {
                _toastNotification.AddErrorToastMessage("Nie udało się pobrać użytkowników, spróbuj ponownie!",
                    new ToastrOptions { Title = "Błąd!" });
                _logger.LogError(e, "Nie udało się pobrać użytkowników. Users - panel administratora.");
            }

            return View(model);
        }

        [Route("statystyki-google")]
        public IActionResult GoogleAnalytics()
        {
            return View();
        }
        #endregion

        #region Akcje

        [Route("aktualizuj-wynik")]
        public IActionResult UpdateScore(Match model)
        {
            try
            {
                _adminService.UpdateMatchScore(model);
                _statsService.UpdateUserPointsAndPositions(model.StartDate);

                return Json(new { ok = true });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się zaktualizować wyniku.");
                return Json(new { ok = false, message = "Nie udało się zaktualizować wyniku. Spróbuj ponownie." });
            }
        }
        
        [Route("dodaj-mecz")]
        public IActionResult PostNewMatch(Match model, int homeId, int awayId)
        {
            try
            {
                model.StartDate = model.StartDate.AddSeconds(-model.StartDate.Second);
                model.StartDate = model.StartDate.AddMilliseconds(-model.StartDate.Millisecond);
                model.HomeTeam = _adminService.GetTeamById(homeId);
                model.AwayTeam = _adminService.GetTeamById(awayId);

                _adminService.AddMatch(model);
                return Json(new { ok = true });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się dodać nowego meczu.");
                return Json(new { ok = false, message = "Nie udało się dodać nowego meczu. Spróbuj ponownie." });
            }
        }
        
        [Route("usun-mecz")]
        public IActionResult DeleteMatch(int id)
        {
            try
            {
                var match = _adminService.GetMatch(id);
                if (match.Bets.Any())
                {
                    TempData["Message"] = "WARNING";
                    return RedirectToAction("ListOfMatches");
                }

                _adminService.DeleteMatch(id);
                TempData["Message"] = null;
                return RedirectToAction("ListOfMatches");
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się usunąć meczu. DeleteMatch - panel administratora.");
                TempData["Message"] = "ERROR";
                return RedirectToAction("ListOfMatches");
            }
        }
        
        [Route("aktualizuj-status-wyeliminowany")]
        public IActionResult UpdateEliminatedStatus(Team model)
        {
            try
            {
                _adminService.UpdateEliminatedStatus(model);
                return Json(new { ok = true });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się zmienić stanu eliminacji.");
                return Json(new { ok = false, message = "Nie udało się zmienić stanu eliminacji. Spróbuj ponownie." });
            }
        }
        
        [Route("aktualizuj-status-fun-user")]
        public IActionResult UpdateFunUser(ApplicationUser model)
        {
            try
            {
                _adminService.UpdateFunUser(model);
                return Json(new { ok = true });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się zmienić statusu Fun Account.");
                return Json(new { ok = false, message = "Nie udało się zmienić statusu Fun Account.. Spróbuj ponownie." });
            }
        }

        [Route("usun-uzytkownika")]
        public IActionResult DeleteUser(string id, bool delete)
        {
            try
            {
                if (!delete) return Json(new {ok = false, message = "Czy na pewno chcesz usunąć tego użytkownika ?"});
                _adminService.DeleteUser(id);
                return Json(new {ok = true});
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Nie udało się usunąć użytkownika.");
                return Json(new { ok = false, message = "Nie udało się usunąć użytkownika.. Spróbuj ponownie." });
            }
        }
        #endregion
        
    }
}