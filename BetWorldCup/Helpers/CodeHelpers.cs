﻿using System.Text;

namespace BetWorldCup.Helpers
{
    public static class CodeHelpers
    {
        public static string RemoveDiacritics(this string s) => Encoding.ASCII.GetString(Encoding.GetEncoding("Cyrillic").GetBytes(s)).Replace(" ", "");
    }
}
