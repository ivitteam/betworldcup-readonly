﻿namespace BetWorldCup.Helpers
{
    public static class BetHelpers
    {
        public static string GetResultType(int homeScore, int awayScore)
        {
            return homeScore == awayScore ? "DRAW" : homeScore > awayScore ? "WIN" : "LOSE";
        }

        //TODO: do przetestowania o ile będzie potrzebne
        public static void ColorIndexing(ref int firstIndex, ref int secondIndex, ref int thirdIndex)
        {
            if (firstIndex < 160)
            {
                if (secondIndex > 215)
                {
                    if (thirdIndex > 215)
                    {
                        firstIndex = 255;
                        secondIndex = 0;
                        thirdIndex = 0;
                    }
                    else
                    {
                        firstIndex = 255;
                        thirdIndex += 40;
                        secondIndex = 0;
                    }
                }
                else
                {
                    firstIndex = 255;
                    secondIndex += 40;
                    thirdIndex = 0;
                }
            }
            else
            {
                firstIndex -= 30;
            }
        }
    }
}
