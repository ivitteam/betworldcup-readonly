﻿using System.IO;
using BetWorldCup.Data;
using BetWorldCup.Extensions;
using BetWorldCup.Models;
using BetWorldCup.Services;
using BetWorldCup.Services.Repository;
using BetWorldCup.Services.RESTApi;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NToastNotify;

namespace BetWorldCup
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
                {
                    // Password settings
                    options.Password.RequireDigit = true;
                    options.Password.RequiredLength = 8;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequiredUniqueChars = 2;

                    // Signin settings
                    options.SignIn.RequireConfirmedEmail = true;

                    // User settings
                    options.User.RequireUniqueEmail = true;
                    
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            //Redirect do zaloguj
            services.ConfigureApplicationCookie(options => options.LoginPath = "/zaloguj");

            //Google auth
            services.AddAuthentication().AddGoogle(googleOptions =>
            {
                googleOptions.ClientId = "ClientId";
                googleOptions.ClientSecret = "ClientSecret";
            });

            //Facebook auth
            services.AddAuthentication().AddFacebook(facebookOptions =>
            {
                facebookOptions.AppId = "AppId";
                facebookOptions.AppSecret = "AppSecret";
            });

            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IMatchRepository, MatchRepository>();
            services.AddTransient<IBetRepository, BetRepository>();
            services.AddTransient<IHomeService, HomeService>();
            services.AddTransient<IStatsService, StatsService>();
            services.AddTransient<IAdminService, AdminService>();
            services.AddTransient<IStandingsRepository, StandingsRepository>();
            services.AddTransient<ILiveScoresService, LiveScoresService>();
            services.AddTransient<IGoogleAnalyticsApiService, GoogleAnalyticsApiService>();
            services.AddTransient<ISitemapBuilderService, SitemapBuilderService>();
            services.AddTransient<IChatService, ChatService>();
            services.AddTransient<IGroupTableService, GroupTableService>();

            services.AddSingleton<IHostedService, ScoreUpdater>();

            services.AddMvc().AddNToastNotifyToastr(new ToastrOptions
            {
                PositionClass = ToastPositions.TopFullWidth
            });

            services.AddMvc();
            services.AddMvc(options =>
            {
                options.Filters.Add(new RequireHttpsAttribute());
            });

            services.AddDataProtection()
                .SetApplicationName("betworldcup")
                .PersistKeysToFileSystem(new DirectoryInfo(@"./wwwroot/shared-keys/"));

            services.AddMemoryCache();
            services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>, ApplicationUserClaimsPrincipalFactory>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseAspNetCoreExceptionHandler();
            }

            app.UseStatusCodePagesWithReExecute("/error", "?statusCode={0}");

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseWebSockets();
            app.UseMiddleware<ChatWebSocketMiddleware>();
            app.UseNToastNotify();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
