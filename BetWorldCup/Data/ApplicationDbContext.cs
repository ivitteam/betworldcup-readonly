﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using BetWorldCup.Models;
using BetWorldCup.Models.BetViewModels;
using BetWorldCup.Models.StatsViewModels;

namespace BetWorldCup.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Match> Matches { get; set; }
        public DbSet<Bet> Bets { get; set; }
        public DbSet<BetResult> BetResults { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<ResultType> ResultTypes { get; set; }
        public DbSet<UserStandings> UserStandings{ get; set; }
        public DbSet<ChatDbRow> ChatRows{ get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        // ReSharper disable once RedundantOverriddenMember
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>()
                .HasIndex(u => u.Name)
                .IsUnique();
        }
    }
}
