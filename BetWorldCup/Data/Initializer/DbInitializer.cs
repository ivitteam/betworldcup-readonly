﻿using System;
using System.Collections.Generic;
using System.Linq;
using BetWorldCup.Models;
using BetWorldCup.Models.BetViewModels;
using BetWorldCup.Services;
using Microsoft.AspNetCore.Identity;

namespace BetWorldCup.Data.Initializer
{
    public static class DbInitializer
    {

        public static void SeedData(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IStatsService statsService)
        {
            if (!context.Roles.Any())
            {
                var role = new IdentityRole
                {
                    Name = "ADMIN",
                    NormalizedName = "ADMIN"
                };

                roleManager.CreateAsync(role).Wait();
            }

            if (!context.Users.Any())
            {
                var user = new ApplicationUser
                {
                    UserName = "ivaan84@gmail.com",
                    Email = "ivaan84@gmail.com",
                    NormalizedEmail = "IVAAN84@GMAIL.COM",
                    NormalizedUserName = "IVAAN84@GMAIL.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "ivaanGG",
                    Points = 0,
                    Position = 999
                };

                var result = userManager.CreateAsync(user, "para$OLKA1").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "ADMIN").Wait();
                }

                user = new ApplicationUser
                {
                    UserName = "kubak3383@gmail.com",
                    Email = "kubak3383@gmail.com",
                    NormalizedEmail = "KUBAK3383@GMAIL.COM",
                    NormalizedUserName = "KUBAK3383@GMAIL.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "kubak",
                    Points = 0,
                    Position = 999
                };

                result = userManager.CreateAsync(user, "kubak783120").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "ADMIN").Wait();
                }

                user = new ApplicationUser
                {
                    UserName = "marek.szupienko@gmail.com",
                    Email = "marek.szupienko@gmail.com",
                    NormalizedEmail = "MAREK.SZUPIENKO@GMAIL.COM",
                    NormalizedUserName = "MAREK.SZUPIENKO@GMAIL.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "marek.szupienko",
                    Points = 0,
                    Position = 999
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "ADMIN").Wait();
                }
            }

            if (!context.ResultTypes.Any())
            {
                var resultTypes = new List<ResultType>
                    {
                        new ResultType
                        {
                            Type = "WIN"
                        },
                        new ResultType
                        {
                            Type = "DRAW"
                        },
                        new ResultType
                        {
                            Type = "LOSE"
                        }
                    };
                context.ResultTypes.AddRange(resultTypes);
                context.SaveChanges();
            }

            if (!context.BetResults.Any())
            {
                var betResultTypes = new List<BetResult>
                    {
                        new BetResult
                        {
                            Type = "SCORE_MATCH",
                            Points = 15
                        },
                        new BetResult
                        {
                            Type = "RESULT_MATCH",
                            Points = 5
                        },
                        new BetResult
                        {
                            Type = "NO_MATCH",
                            Points = 1
                        },
                    };
                context.BetResults.AddRange(betResultTypes);
                context.SaveChanges();
            }

            if (!context.Teams.Any())
            {
                var teams = new List<Team>
                {
                    //UEFA
                    new Team {LongName = "Rosja",APIName = "Russia", Multiplier = 1, ShortName = "RUS", APIId = "1431"},
                    new Team {LongName = "Francja",APIName = "France", Multiplier = 1, ShortName = "FRA", APIId = "1439"},
                    new Team {LongName = "Portugalia",APIName = "Portugal", Multiplier = 1, ShortName = "POR", APIId = "1437"},
                    new Team {LongName = "Niemcy",APIName = "Germany", Multiplier = 1, ShortName = "GER", APIId = "1449"},
                    new Team {LongName = "Serbia",APIName = "Serbia", Multiplier = 1, ShortName = "SRB", APIId = "1447"},
                    new Team {LongName = "Polska",APIName = "Poland", Multiplier = 2, ShortName = "POL", APIId = "1459"},
                    new Team {LongName = "Anglia",APIName = "England", Multiplier = 1, ShortName = "ENG", APIId = "1456"},
                    new Team {LongName = "Hiszpania",APIName = "Spain", Multiplier = 1, ShortName = "SPA", APIId = "1438"},
                    new Team {LongName = "Belgia",APIName = "Belgium", Multiplier = 1, ShortName = "BEL", APIId = "1453"},
                    new Team {LongName = "Islandia",APIName = "Iceland", Multiplier = 1, ShortName = "ISL", APIId = "1444"},
                    new Team {LongName = "Szwecja",APIName = "Sweden", Multiplier = 1, ShortName = "SWE", APIId = "1451"},
                    new Team {LongName = "Chorwacja",APIName = "Croatia", Multiplier = 1, ShortName = "CRO", APIId = "211"},
                    new Team {LongName = "Szwajcaria",APIName = "Switzerland", Multiplier = 1, ShortName = "SUI", APIId = "208"},
                    new Team {LongName = "Dania",APIName = "Denmark", Multiplier = 1, ShortName = "DEN", APIId = "1442"},


                    //CAF
                    new Team {LongName = "Nigeria",APIName = "Nigeria", Multiplier = 1, ShortName = "NIG", APIId = "1445"},
                    new Team {LongName = "Senegal",APIName = "Senegal", Multiplier = 1, ShortName = "SNG", APIId = "1460"},
                    new Team {LongName = "Egipt",APIName = "Egypt", Multiplier = 1, ShortName = "EGT", APIId = "215"},
                    new Team {LongName = "Maroko",APIName = "Morroco", Multiplier = 1, ShortName = "MAR", APIId = "1435"},
                    new Team {LongName = "Tunezja",APIName = "Tunisia", Multiplier = 1, ShortName = "TUN", APIId = "1455"},


                    //AFC
                    new Team {LongName = "Iran",APIName = "Iran", Multiplier = 1, ShortName = "IRA", APIId = "1436"},
                    new Team {LongName = "Japonia",APIName = "Japan", Multiplier = 1, ShortName = "JAP", APIId = "1458"},
                    new Team {LongName = "Korea Południowa",APIName = "South Korea", Multiplier = 1, ShortName = "KPD", APIId = "1452"},
                    new Team {LongName = "Arabia Saudyjska",APIName = "Saudi Arabia", Multiplier = 1, ShortName = "ASD", APIId = "1432"},
                    new Team {LongName = "Australia",APIName = "Australia", Multiplier = 1, ShortName = "AUT", APIId = "1440"},


                    //CONMEBOL
                    new Team {LongName = "Brazylia",APIName = "Brazil", Multiplier = 1, ShortName = "BRA", APIId = "1448"},
                    new Team {LongName = "Urugwaj",APIName = "Uruguay", Multiplier = 1, ShortName = "URU", APIId = "1434"},
                    new Team {LongName = "Argentyna",APIName = "Argentina", Multiplier = 1, ShortName = "ARG", APIId = "1443"},
                    new Team {LongName = "Kolumbia",APIName = "Colombia", Multiplier = 1, ShortName = "COL", APIId = "1457"},
                    new Team {LongName = "Peru",APIName = "Peru", Multiplier = 1, ShortName = "PER", APIId = "1441"},


                    //CONCACAF
                    new Team {LongName = "Meksyk",APIName = "Mexico", Multiplier = 1, ShortName = "MEX", APIId = "1450"},
                    new Team {LongName = "Kostaryka",APIName = "Costa Rica", Multiplier = 1, ShortName = "COS", APIId = "1446"},
                    new Team {LongName = "Panama",APIName = "Panama", Multiplier = 1, ShortName = "PAN", APIId = "1454"}

                };
                context.Teams.AddRange(teams);
                context.SaveChanges();
            }

            #region POPRAWNY SEED

            //if (!context.Matches.Any())
            //{
            //    var matches = new List<Match>
            //    {
            //        //KOLEJKA 1
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "RUS") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "ASD"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 14, 17, 0, 0),
            //            ApiId = 856735,
            //            LeagueId = 793
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "EGT") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "URU"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 15, 14, 0, 0),
            //            ApiId = 856736,
            //            LeagueId = 793
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "MAR") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "IRA"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 15, 17, 0, 0),
            //            ApiId = 856741,
            //            LeagueId = 794
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "POR") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "SPA"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 15, 20, 0, 0),
            //            ApiId = 856742,
            //            LeagueId = 794
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "FRA") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "AUT"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 16, 12, 0, 0),
            //            ApiId = 856729,
            //            LeagueId = 795
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "ARG") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "ISL"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 16, 15, 0, 0),
            //            ApiId = 856747,
            //            LeagueId = 796
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "PER") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "DEN"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 16, 18, 0, 0),
            //            ApiId = 856730,
            //            LeagueId = 795
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "CRO") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "NIG"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 16, 21, 0, 0),
            //            ApiId = 856748,
            //            LeagueId = 796
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "COS") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "SRB"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 17, 14, 0, 0),
            //            ApiId = 856753,
            //            LeagueId = 797
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "GER") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "MEX"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 17, 17, 0, 0),
            //            ApiId = 856723,
            //            LeagueId = 798
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "BRA") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "SUI"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 17, 20, 0, 0),
            //            ApiId = 856754,
            //            LeagueId = 797
            //        },

            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "SWE") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "KPD"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 18, 14, 0, 0),
            //            ApiId = 856724,
            //            LeagueId = 798
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "BEL") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "PAN"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 18, 17, 0, 0),
            //            ApiId = 856759,
            //            LeagueId = 799
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "TUN") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "ENG"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 18, 20, 0, 0),
            //            ApiId = 856760,
            //            LeagueId = 799
            //        }
            //        ,new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "COL") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "JAP"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 19, 14, 0, 0),
            //            ApiId = 856717,
            //            LeagueId = 800
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "POL") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "SNG"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 19, 17, 0, 0),
            //            ApiId = 856718,
            //            LeagueId = 800
            //        },

            //        //KOLEJKA 2
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "RUS") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "EGT"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 19, 20, 0, 0),
            //            ApiId = 856737,
            //            LeagueId = 793
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "POR") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "MAR"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 20, 14, 0, 0),
            //            ApiId = 856743,
            //            LeagueId = 794
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "URU") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "ASD"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 20, 17, 0, 0),
            //            ApiId = 856738,
            //            LeagueId = 793
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "IRA") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "SPA"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 20, 20, 0, 0),
            //            ApiId = 856744,
            //            LeagueId = 794
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "DEN") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "AUT"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 21, 14, 0, 0),
            //            ApiId = 856731,
            //            LeagueId = 795
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "FRA") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "PER"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 21, 17, 0, 0),
            //            ApiId = 856732,
            //            LeagueId = 795
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "ARG") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "CRO"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 21, 20, 0, 0),
            //            ApiId = 856749,
            //            LeagueId = 796
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "BRA") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "COS"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 22, 14, 0, 0),
            //            ApiId = 856755,
            //            LeagueId = 797
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "NIG") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "ISL"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 22, 17, 0, 0),
            //            ApiId = 856750,
            //            LeagueId = 796
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "SRB") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "SUI"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 22, 20, 0, 0),
            //            ApiId = 856756,
            //            LeagueId = 797
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "BEL") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "TUN"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 23, 14, 0, 0),
            //            ApiId = 856761,
            //            LeagueId = 799
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "KPD") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "MEX"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 23, 17, 0, 0),
            //            ApiId = 856725,
            //            LeagueId = 798
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "GER") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "SWE"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 23, 20, 0, 0),
            //            ApiId = 856726,
            //            LeagueId = 798
            //        },

            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "ENG") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "PAN"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 24, 14, 0, 0),
            //            ApiId = 856762,
            //            LeagueId = 799
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "JAP") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "SNG"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 24, 17, 0, 0),
            //            ApiId = 856719,
            //            LeagueId = 800
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "POL") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "COL"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 24, 20, 0, 0),
            //            ApiId = 856720,
            //            LeagueId = 800
            //        },

            //        //KOLEJKA 3
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "ASD") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "EGT"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 25, 16, 0, 0),
            //            ApiId = 856740,
            //            LeagueId = 793
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "URU") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "RUS"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 25, 16, 0, 0),
            //            ApiId = 856739,
            //            LeagueId = 793
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "IRA") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "POR"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 25, 20, 0, 0),
            //            ApiId = 856745,
            //            LeagueId = 794
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "SPA") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "MAR"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 25, 20, 0, 0),
            //            ApiId = 856746,
            //            LeagueId = 794
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "AUT") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "PER"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 26, 16, 0, 0),
            //            ApiId = 856734,
            //            LeagueId = 795
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "DEN") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "FRA"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 26, 16, 0, 0),
            //            ApiId = 856733,
            //            LeagueId = 795
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "NIG") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "ARG"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 26, 20, 0, 0),
            //            ApiId = 856751,
            //            LeagueId = 796
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "ISL") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "CRO"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 26, 20, 0, 0),
            //            ApiId = 856752,
            //            LeagueId = 796
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "KPD") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "GER"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 27, 16, 0, 0),
            //            ApiId = 856727,
            //            LeagueId = 798
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "MEX") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "SWE"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 27, 16, 0, 0),
            //            ApiId = 856728,
            //            LeagueId = 798
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "SUI") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "COS"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 27, 20, 0, 0),
            //            ApiId = 856758,
            //            LeagueId = 797
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "SRB") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "BRA"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 27, 20, 0, 0),
            //            ApiId = 856757,
            //            LeagueId = 797
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "SNG") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "COL"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 28, 16, 0, 0),
            //            ApiId = 856722,
            //            LeagueId = 800
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "JAP") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "POL"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 28, 16, 0, 0),
            //            ApiId = 856721,
            //            LeagueId = 800
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "ENG") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "BEL"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 28, 20, 0, 0),
            //            ApiId = 856763,
            //            LeagueId = 799
            //        },
            //        new Match
            //        {
            //            HomeTeam = context.Teams.Single(p => p.ShortName == "PAN") ,
            //            AwayTeam = context.Teams.Single(p => p.ShortName == "TUN"),
            //            Multiplier = 1,
            //            StartDate = new DateTime(2018, 06, 28, 20, 0, 0),
            //            ApiId = 856764,
            //            LeagueId = 799
            //        }
            //    };

            //    context.Matches.AddRange(matches);
            //    context.SaveChanges();
            //}

            #endregion

            #region SEED TESTOWY - DO DEVELOPMENTU

            var random = new Random();

            if (!context.Matches.Any())
            {
                var matches = new List<Match>
                {
                    //KOLEJKA 1
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "RUS") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "ASD"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-7),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 793,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "EGT") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "URU"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-7),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 793,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "MAR") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "IRA"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-6),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 794,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "POR") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "SPA"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-6),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 794,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "FRA") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "AUT"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-5),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 795,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "ARG") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "ISL"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-5),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 796,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "PER") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "DEN"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-4),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 795,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "CRO") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "NIG"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-4),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 796,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "COS") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "SRB"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-3),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 797,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "GER") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "MEX"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-3),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 798,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "BRA") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "SUI"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-2),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 797,
                        IsFinished = true
                    },

                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "SWE") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "KPD"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-2),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 798,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "BEL") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "PAN"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-1),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 799,
                        IsFinished = true
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "TUN") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "ENG"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(-1),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 799,
                        IsFinished = true
                    }
                    ,new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "COL") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "JAP"),
                        Multiplier = 1,
                        StartDate = DateTime.Now,
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 800
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "POL") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "SNG"),
                        Multiplier = 1,
                        StartDate = DateTime.Now,
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 800
                    },

                    //KOLEJKA 2
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "RUS") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "EGT"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(1),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 793
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "POR") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "MAR"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(1),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 794
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "URU") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "ASD"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(2),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 793
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "IRA") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "SPA"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(2),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 794
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "DEN") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "AUT"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(3),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 795
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "FRA") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "PER"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(3),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 795
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "ARG") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "CRO"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(4),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 796
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "BRA") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "COS"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(4),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 797
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "NIG") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "ISL"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(5),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 796
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "SRB") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "SUI"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(5),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 797
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "BEL") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "TUN"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(6),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 799
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "KPD") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "MEX"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(6),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 798
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "GER") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "SWE"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(7),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 798
                    },

                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "ENG") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "PAN"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(7),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 799
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "JAP") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "SNG"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(8),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 800
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "POL") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "COL"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(8),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 800
                    },

                    //KOLEJKA 3
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "ASD") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "EGT"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(9),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 793
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "URU") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "RUS"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(9),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 793
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "IRA") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "POR"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(10),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 794
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "SPA") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "MAR"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(10),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 794
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "AUT") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "PER"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(11),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 795
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "DEN") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "FRA"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(11),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 795
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "NIG") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "ARG"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(12),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 796
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "ISL") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "CRO"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(12),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 796
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "KPD") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "GER"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(13),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 798
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "MEX") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "SWE"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(13),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 798
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "SUI") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "COS"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(14),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 797
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "SRB") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "BRA"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(14),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 797
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "SNG") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "COL"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(15),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 800
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "JAP") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "POL"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(15),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 800
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "ENG") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "BEL"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(16),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 799
                    },
                    new Match
                    {
                        HomeTeam = context.Teams.Single(p => p.ShortName == "PAN") ,
                        AwayTeam = context.Teams.Single(p => p.ShortName == "TUN"),
                        Multiplier = 1,
                        StartDate = DateTime.Now.AddDays(16),
                        HomeScore = random.Next(0, 5),
                        AwayScore = random.Next(0, 5),
                        LeagueId = 799
                    }
                };

                context.Matches.AddRange(matches);
                context.SaveChanges();
            }

            if (context.Users.Count() < 4)
            {
                var user = new ApplicationUser
                {
                    UserName = "test1@test.com",
                    Email = "test1@test.com",
                    NormalizedEmail = "TEST1@TEST.COM",
                    NormalizedUserName = "TEST1@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test1"
                };

                var result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test2@test.com",
                    Email = "test2@test.com",
                    NormalizedEmail = "TEST2@TEST.COM",
                    NormalizedUserName = "TEST2@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test2"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test3@test.com",
                    Email = "test3@test.com",
                    NormalizedEmail = "TEST3@TEST.COM",
                    NormalizedUserName = "TEST3@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test3"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test4@test.com",
                    Email = "test4@test.com",
                    NormalizedEmail = "TEST4@TEST.COM",
                    NormalizedUserName = "TEST4@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test4"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test5@test.com",
                    Email = "test5@test.com",
                    NormalizedEmail = "TEST5@TEST.COM",
                    NormalizedUserName = "TEST5@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test5"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test6@test.com",
                    Email = "test6@test.com",
                    NormalizedEmail = "TEST6@TEST.COM",
                    NormalizedUserName = "TEST6@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test6"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test7@test.com",
                    Email = "test7@test.com",
                    NormalizedEmail = "TEST7@TEST.COM",
                    NormalizedUserName = "TEST7@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test7"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test8@test.com",
                    Email = "test8@test.com",
                    NormalizedEmail = "TEST8@TEST.COM",
                    NormalizedUserName = "TEST8@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test8"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test9@test.com",
                    Email = "test9@test.com",
                    NormalizedEmail = "TEST9@TEST.COM",
                    NormalizedUserName = "TEST9@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test9"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test10@test.com",
                    Email = "test10@test.com",
                    NormalizedEmail = "TEST10@TEST.COM",
                    NormalizedUserName = "TEST10@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test10"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test11@test.com",
                    Email = "test11@test.com",
                    NormalizedEmail = "TEST11@TEST.COM",
                    NormalizedUserName = "TEST11@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test11"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test12@test.com",
                    Email = "test12@test.com",
                    NormalizedEmail = "TEST12@TEST.COM",
                    NormalizedUserName = "TEST12@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test12"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test13@test.com",
                    Email = "test13@test.com",
                    NormalizedEmail = "TEST13@TEST.COM",
                    NormalizedUserName = "TEST13@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test13"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test14@test.com",
                    Email = "test14@test.com",
                    NormalizedEmail = "TEST14@TEST.COM",
                    NormalizedUserName = "TEST14@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test14"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test15@test.com",
                    Email = "test15@test.com",
                    NormalizedEmail = "TEST15@TEST.COM",
                    NormalizedUserName = "TEST15@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test15"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test16@test.com",
                    Email = "test16@test.com",
                    NormalizedEmail = "TEST16@TEST.COM",
                    NormalizedUserName = "TEST16@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test16"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test17@test.com",
                    Email = "test17@test.com",
                    NormalizedEmail = "TEST17@TEST.COM",
                    NormalizedUserName = "TEST17@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test17"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test18@test.com",
                    Email = "test18@test.com",
                    NormalizedEmail = "TEST18@TEST.COM",
                    NormalizedUserName = "TEST18@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test18"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test19@test.com",
                    Email = "test19@test.com",
                    NormalizedEmail = "TEST19@TEST.COM",
                    NormalizedUserName = "TEST19@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test19"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test20@test.com",
                    Email = "test20@test.com",
                    NormalizedEmail = "TEST20@TEST.COM",
                    NormalizedUserName = "TEST20@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test20"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test21@test.com",
                    Email = "test21@test.com",
                    NormalizedEmail = "TEST21@TEST.COM",
                    NormalizedUserName = "TEST21@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test21"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test22@test.com",
                    Email = "test22@test.com",
                    NormalizedEmail = "TEST22@TEST.COM",
                    NormalizedUserName = "TEST22@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test22"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test23@test.com",
                    Email = "test23@test.com",
                    NormalizedEmail = "TEST23@TEST.COM",
                    NormalizedUserName = "TEST23@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test23"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test24@test.com",
                    Email = "test24@test.com",
                    NormalizedEmail = "TEST24@TEST.COM",
                    NormalizedUserName = "TEST24@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test24"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test25@test.com",
                    Email = "test25@test.com",
                    NormalizedEmail = "TEST25@TEST.COM",
                    NormalizedUserName = "TEST25@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test25"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test26@test.com",
                    Email = "test26@test.com",
                    NormalizedEmail = "TEST26@TEST.COM",
                    NormalizedUserName = "TEST26@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test26"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test27@test.com",
                    Email = "test27@test.com",
                    NormalizedEmail = "TEST27@TEST.COM",
                    NormalizedUserName = "TEST27@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test27"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test28@test.com",
                    Email = "test28@test.com",
                    NormalizedEmail = "TEST28@TEST.COM",
                    NormalizedUserName = "TEST28@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test28"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test29@test.com",
                    Email = "test29@test.com",
                    NormalizedEmail = "TEST29@TEST.COM",
                    NormalizedUserName = "TEST29@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test29"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());

                user = new ApplicationUser
                {
                    UserName = "test30@test.com",
                    Email = "test30@test.com",
                    NormalizedEmail = "TEST30@TEST.COM",
                    NormalizedUserName = "TEST30@TEST.COM",
                    FunAccount = false,
                    EmailConfirmed = true,
                    Name = "test30"
                };

                result = userManager.CreateAsync(user, "/;p0()_+").Result;
                Console.WriteLine(result.ToString());
            }

            if (!context.Bets.Any())
            {
                var bets = (from user in userManager.Users
                            from match in context.Matches
                            select new Bet
                            {
                                User = user,
                                Match = match,
                                HomeScore = random.Next(0, 5),
                                AwayScore = random.Next(0, 5)
                            }).ToList();


                foreach (var bet in bets)
                {
                    if (bet.HomeScore == bet.AwayScore)
                        bet.Result = context.ResultTypes.Single(p => p.Type == "DRAW");
                    else if (bet.HomeScore > bet.AwayScore)
                        bet.Result = context.ResultTypes.Single(p => p.Type == "WIN");
                    else
                        bet.Result = context.ResultTypes.Single(p => p.Type == "LOSE");
                }

                context.Bets.AddRange(bets);
                context.SaveChanges();


                foreach (var match in context.Matches.Where(p => p.StartDate <= DateTime.Now))
                {
                    if (match.HomeScore == match.AwayScore)
                        match.Result = context.ResultTypes.Single(p => p.Type == "DRAW");
                    else if (match.HomeScore > match.AwayScore)
                        match.Result = context.ResultTypes.Single(p => p.Type == "WIN");
                    else
                        match.Result = context.ResultTypes.Single(p => p.Type == "LOSE");

                    foreach (var bet in context.Bets.Where(p => p.Match.MatchId == match.MatchId))
                    {
                        if (match.Result != bet.Result)
                            bet.BetResult = context.BetResults.Single(p => p.Type == "NO_MATCH");
                        else if (match.HomeScore != bet.HomeScore || match.AwayScore != bet.AwayScore)
                            bet.BetResult = context.BetResults.Single(p => p.Type == "RESULT_MATCH");
                        else
                            bet.BetResult = context.BetResults.Single(p => p.Type == "SCORE_MATCH");
                    }
                }

                context.SaveChanges();
            }

            if (!context.UserStandings.Any())
            {
                var dates = statsService.GetPlayedMatchesDays();

                foreach (var date in dates)
                {
                    statsService.UpdateUserPointsAndPositionsForSeed(date);
                }
            }

            #endregion

        }
    }

}
