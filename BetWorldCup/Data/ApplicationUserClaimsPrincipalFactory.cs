﻿using System.Security.Claims;
using System.Threading.Tasks;
using BetWorldCup.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace BetWorldCup.Data
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ApplicationUserClaimsPrincipalFactory : UserClaimsPrincipalFactory<ApplicationUser, IdentityRole>
    {
        public ApplicationUserClaimsPrincipalFactory(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IOptions<IdentityOptions> optionsAccessor) : base(userManager, roleManager, optionsAccessor)
        {
        }

        public override async Task<ClaimsPrincipal> CreateAsync(ApplicationUser user)
        {
            var principal = await base.CreateAsync(user);

            //Putting our Property to Claims
            //I'm using ClaimType.Email, but you may use any other or your own
            ((ClaimsIdentity)principal.Identity).AddClaims(new[] {
                new Claim(ClaimTypes.GivenName, user.Name)
            });

            ((ClaimsIdentity)principal.Identity).AddClaims(new[] {
                new Claim(ClaimTypes.System, user.FunAccount.ToString())
            });

            return principal;
        }
    }
}
