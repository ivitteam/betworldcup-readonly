﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace BetWorldCup.Data.Migrations
{
    public partial class APIId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "APIName",
                table: "Teams",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 20);

            migrationBuilder.AddColumn<string>(
                name: "APIId",
                table: "Teams",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "APIId",
                table: "Teams");

            migrationBuilder.AlterColumn<string>(
                name: "APIName",
                table: "Teams",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
