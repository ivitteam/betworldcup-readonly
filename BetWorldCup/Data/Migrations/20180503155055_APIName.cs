﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace BetWorldCup.Data.Migrations
{
    public partial class APIName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApiId",
                table: "Matches");

            migrationBuilder.AddColumn<string>(
                name: "APIName",
                table: "Teams",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "APIName",
                table: "Teams");

            migrationBuilder.AddColumn<int>(
                name: "ApiId",
                table: "Matches",
                nullable: false,
                defaultValue: 0);
        }
    }
}
